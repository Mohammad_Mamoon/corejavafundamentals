/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionOverload;

/**
 *
 * @author Mohammad_Mamoon
 */
public class FuncOverload {
    public static void main(String[] args) {
        
        FuncOverload obj = new FuncOverload();
        obj.message();
        obj.message(10);
    }
    
    public void message() {
        System.out.println("Hello there");
    }
    
    public void message(int number) {
        System.out.println("hello there with age " + number);
    }
    
}
