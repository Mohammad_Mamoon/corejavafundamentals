/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.unsynchronizedWay;

import java.security.SecureRandom;
import java.util.Arrays;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SimpleArray {
    private static final SecureRandom generator = new SecureRandom();
    private final int[] array; // the shared integer array
    private int writeIndex = 0; // shared index of the next element to write to

    public SimpleArray(int size) {
        array = new int[size];
    }
    
    public void add(int value) {
        int position = writeIndex;
        
        try {
            Thread.sleep(generator.nextInt(500));
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        
        // put value in the appropriate element
        array[position] = value;
        System.out.printf("%s wrote %2d to element %d%n", 
                 Thread.currentThread().getName(), value, position); 
        
        // increment the index of element to be written next
        ++writeIndex;
        System.out.printf("Next write index: %d%n", writeIndex);
    }   
    
    // used for outputting the contents of the shared integer array
    public String toString() {
          return Arrays.toString(array); 
     }
    
    
    
}
