/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.unsynchronizedWay;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SharedArrayTest {
    public static void main(String[] args) {
        
        // construct the shared object
        SimpleArray sharedSimpleArray = new SimpleArray(6);
        
        // create two tasks to write to the shared object
        ArrayWriter writer1 = new ArrayWriter(1,sharedSimpleArray);
        ArrayWriter writer2 = new ArrayWriter(11,sharedSimpleArray);
        
        // Execute the tasks with an Executor Service
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        executorService.execute(writer1);
        executorService.execute(writer2);
        
        executorService.shutdown();
        
        try {
            // wait 1 minute for both the tasks to finish
            boolean tasksEnded =
                    executorService.awaitTermination(1, TimeUnit.MINUTES);
            
            if(tasksEnded) {
                System.out.printf("%nContents of the simpleArray:%n");
                System.out.println(sharedSimpleArray);
            }
            
            else
                System.out.println("Timed out while waiting for tasks to finish");
                
        }catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
}
