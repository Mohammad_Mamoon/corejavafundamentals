/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.producerCosumsrSyncArrayBlockingQueue;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mohammad_Mamoon
 */
public class BlockingBufferTest {
    public static void main(String[] args) throws InterruptedException {
        
        // create new thread pool with two threads
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        // create BlockingBuffer to store ints
        Buffer sharedLocation = new BlockingBuffer();
        
        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));
        
        executorService.shutdown();
        
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        
    }
    
}
