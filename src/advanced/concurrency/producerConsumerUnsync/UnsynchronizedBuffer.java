/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.producerConsumerUnsync;

/**
 *
 * @author Mohammad_Mamoon
 */
public class UnsynchronizedBuffer implements Buffer {
    
    private int buffer = -1; // shared by producer and consumer threads
    
    // place value into buffer
    public void blockingPut(int value) throws InterruptedException {
        System.out.printf("Producer writes\t%2d", value);
        buffer = value;
    }
    
    // return value from buffer
    public int blockingGet() throws InterruptedException {
        System.out.printf("Consumer reads\t%2d", buffer);
        return buffer;
    }
    
}
