/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.producerConsumerUnsync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SharedBufferTest {
    public static void main(String[] args) throws InterruptedException {
        
        // create new thread pool with two threads
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        // create UnsynchronizedBuffer to store ints
        Buffer sharedLocation = new UnsynchronizedBuffer();
        
        System.out.println( 
                 "Action\t\tValue\tSum of Produced\tSum of Consumed"); 
        
        System.out.printf(
                 "------\t\t-----\t---------------\t---------------%n%n");
        
        // execute the Producer and Consumer, giving each 
        // access to the sharedLocation
        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));
        
        executorService.shutdown(); // terminate app when tasks complete 
        executorService.awaitTermination(1, TimeUnit.MINUTES); 
    }
    
}
