/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.producerConsumerUnsync;

/**
 *
 * @author Mohammad_Mamoon
 */

// interface Bufffer specifies the methods called by producer and consumer
public interface Buffer {
    
    // place int value into buffer
    public void blockingPut(int value) throws InterruptedException;
    
    // return int value from buffer
    public int blockingGet() throws InterruptedException;
}
