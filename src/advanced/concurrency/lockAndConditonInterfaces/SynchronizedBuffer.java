/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.lockAndConditonInterfaces;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SynchronizedBuffer implements Buffer {
    
    // SynchronizedBuffer object contains the reference to
    // ReentrantLock object that implements the Lock interface
    // Lock to control synchronization with this buffer
    private final Lock accessLock = new ReentrantLock();
    
    
    // here Lock objects are used to explicitly declare
    // Condition objects on which a thread may need to wait
    // in producer/consumer relationship , each work with 
    // different Condition objects
    // which is not possible when using synchronized keywords
    // and an Object's build-in monitor Lock
    // using newCondition method of Lock returns an object that
    // implements the Conditon interface
    // conditions to control reading and writing
    private final Condition canWrite = accessLock.newCondition();
    private final Condition canRead = accessLock.newCondition();
    
    private int buffer = -1; // shared by producer and consumer
    private boolean occupied = false;
    
    
    // place int value into buffer
    public void blockingPut(int value) throws InterruptedException {
        
        // here a thread calls the lock method of accessLock
        accessLock.lock(); // lock this object
        
        // output thread information and buffer information, then wait
        try {
            
            // while buffer is not empty, place thread in waiting state
            while(occupied) {
                System.out.println("Producer tries to write.");
                displayState("Buffer full. Producer waits.");
                canWrite.await();
                        
            }
            
             buffer = value; // set new buffer value
             
             // indicate producer cannot store another value
             // until consumer retrieves current buffer value
             occupied = true;
             
             displayState("Producer writes " + buffer);
             
             // signal any waiting thread to read from buffer
             canRead.signalAll();
        } finally {
            accessLock.unlock();  // unlock this object
        }
    }
    
    
    // return value from buffer
    public int blockingGet() throws InterruptedException {
        
        int readValue = 0;
        accessLock.lock(); // lock this object
        
        // output thread information and buffer information, then wait
        try {
            
            // if there is no data to read, place thread in waiting state
            while(!occupied) {
                System.out.println("Consumer tries to read.");
                displayState("Buffer empty. Consumer waits.");
                canRead.await();
                        
            }
             
             // indicate producer can store another value
             // because consumer just retrieved the buffer value
             occupied = false;
             
             readValue = buffer;
             displayState("Consumer reads " + readValue);
             
             // signal any waiting thread for buffer to be empty
             canWrite.signalAll();
        } finally {
            accessLock.unlock();  // unlock this object
        }
        
        return readValue;
    }
    
    // display current operation and buffer state
    public void displayState(String operation) {
        
        try {
            
            accessLock.lock(); // lock this object
            System.out.printf("%-40s%d\t\t%b%n%n", operation, buffer,
                    occupied);
        } finally {
            accessLock.unlock();
        }
    }
    
}
