/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.producerConsumerSyncUserWay;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SynchronizedBuffer implements Buffer {
    
    private int buffer = -1; // shared by producer and consumer threads
    private boolean occupied = false; 
    
    public synchronized void blockingPut(int value) throws InterruptedException {
        
        // while there are no empty locations, place thread in waiting state
        while(occupied) {
             // output thread information and buffer information, then wait 
             System.out.println("Producer tries to write."); // for demo only
             displayState("Buffer full. Producer waits."); // for demo only
             wait();
        }
        
        buffer = value; // set new buffer value
        
        // indicate producer cannot store another value
        // until consumer consumes current buffer value
        occupied = true;
        
        displayState("Producer writes " + buffer); // for demo only
        notifyAll(); // tell waiting threads to enter Runnable state
        
    } // end of method , releases lock on SynchronizedBuffer object
    
    
    public synchronized int blockingGet() throws InterruptedException {
        
        // while the buffer is empty, place thread in waiting state
        while(!occupied) {
             // output thread information and buffer information, then wait 
             System.out.println("Consumer tries to read."); // for demo only
             displayState("Buffer empty. Consumer waits."); // for demo only
             wait();
        }
       
        
        // indicate producer can store another value
        // beacuse consumer retrieved buffer value
        occupied = false;
        
        displayState("Consumer reads " + buffer); // for demo only
        notifyAll(); // tell waiting threads to enter Runnable state
        
        return buffer;
        
    } // end of method , releases lock on SynchronizedBuffer object
    
    public synchronized void displayState(String operation) {
        
        System.out.printf("%-40s%d\t\t%b%n%n", operation, buffer, 
                occupied);
    }
}
