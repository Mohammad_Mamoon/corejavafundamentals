/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.boundedBuffer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Mohammad_Mamoon
 */ public class CircularBufferTest {
    public static void main(String[] args) throws InterruptedException {
        
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        // create CircularBuffer to store ints
        CircularBuffer sharedLocation = new CircularBuffer();
        
        // display the initial state of the CircularBuffer
        sharedLocation.displayState("Initial State");
        
        
        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));
        
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        
    }
    
}
