/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency.boundedBuffer;


/**
 *
 * @author Mohammad_Mamoon
 */
public class CircularBuffer implements Buffer {
    
    private final int[] buffer =  { -1,-1,-1 }; // shared by producer and consumer threads
    
    private int occupiedCells = 0;
    private int writeIndex = 0;
    private int readIndex = 0;
    
    
    public synchronized void blockingPut(int value) throws InterruptedException {
        
        // wait until buffer has space available then write value
        // while no empty locations, place thread in blocked state
        while(occupiedCells ==  buffer.length) {
             
             System.out.printf("Buffer is full. Producer waits.%n"); 
             wait();
        }
        
        buffer[writeIndex] = value; // set new buffer value
        
       //  update circular writeIndex
       writeIndex = (writeIndex + 1) % buffer.length;
       
       ++occupiedCells; // one more buffer cell is full
       displayState("Producer writes " + value); // for demo only
       notifyAll(); // tell waiting threads to read from buffer
        
    } // end of method , releases lock on CircularBuffer object
    
    
    public synchronized int blockingGet() throws InterruptedException {
        
        // wait until buffer has data , then read value
        // while no data to read, place thread in waiting state
        while(occupiedCells == 0) {
   
             System.out.println("Buffer empty. Consumer waits."); // for demo only
             wait();
        }
       
        
        int readValue = buffer[readIndex];
        
        // update circular read index
        readIndex = (readIndex + 1) % buffer.length;
        
        --occupiedCells;
        displayState("Consumer reads " + readValue); // for demo only
        notifyAll(); // tell waiting threads to write to buffer
        
        return readValue;
        
    } // end of method , releases lock on CircularBuffer object
    
    public synchronized void displayState(String operation) {
        
        // output operation and number of occupied buffer cells
        System.out.printf("%s%s%d)%n%s", operation,
                " (buffer cells occupied: ", occupiedCells, "buffer cells:  ");
        
        for (int value : buffer)
            System.out.printf(" %2d  ", value); // output values in buffer
        
        System.out.printf("%n               ");
        
        for (int i = 0; i < buffer.length; i++)
            System.out.print("---- ");
        
        System.out.printf("%n               ");
        
        for (int i = 0; i < buffer.length; i++) {
            if (i == writeIndex && i == readIndex)
                System.out.print(" WR"); // both write and read index
            else if (i == writeIndex)
                System.out.print(" W   "); // just write index
            else if (i == readIndex)
                System.out.print("  R  "); // just read inde
            else 
                System.out.print("     "); // neither index
        }
        
        System.out.printf("%n%n");
    }
}
