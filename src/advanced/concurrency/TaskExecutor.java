/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TaskExecutor {
    public static void main(String[] args) {
        
        // create and name each runnable
        PrintTask task1 = new PrintTask("task1");
        PrintTask task2 = new PrintTask("task2");
        PrintTask task3 = new PrintTask("task3");
        
        System.out.println("Starting Executor");
        
        // create ExecutorService to manage threads
        ExecutorService executorService = Executors.newCachedThreadPool();
        
        // start the three PrintTaks
        executorService.execute(task1); // start task1
        executorService.execute(task2);
        executorService.execute(task3);
        
        // shutdown ExecutorService -- it decides when to shut down threads
        executorService.shutdown();
      
        
        System.out.printf("Tasks started, main ends.%n%n");
    }
    
}
