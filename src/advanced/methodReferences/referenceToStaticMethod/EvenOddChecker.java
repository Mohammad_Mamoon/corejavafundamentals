/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToStaticMethod;

/**
 *
 * @author Mohammad_Mamoon
 */
public class EvenOddChecker {
    public static boolean isEven(int n) {
        
        return n % 2 == 0;
    }
    
}
