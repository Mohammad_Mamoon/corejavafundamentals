/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToStaticMethod;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MethodReferenceExample {
    public static void main(String[] args) {
        
        List<Integer> myNumbers = Arrays.asList(2,3,45,66,77,12,55);
        
        // Print boolean values using lambda expression
        myNumbers.stream()
                          .filter( (n) -> EvenOddChecker.isEven(n) ) // test method of IntPredicate
                          .forEach((n) -> System.out.println(n));    // accept method of IntConsumer   
                
        
        
        // Print boolean values using method references
        System.out.println("Using method references");
        myNumbers.stream()
                          .filter(EvenOddChecker::isEven)  
                          .forEach(System.out::println);
    }
    
}
