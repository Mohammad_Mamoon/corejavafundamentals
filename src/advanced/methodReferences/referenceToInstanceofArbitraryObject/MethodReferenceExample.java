/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToInstanceofArbitraryObject;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MethodReferenceExample {
    public static void main(String[] args) {
        
        List<String> week = Arrays.asList("Mon","Tue","Wed","Thu","Fri","Sat");
        
        System.out.println("Using lambda expressions");
        week.stream()
                    .map((s) -> s.toUpperCase())
                    .forEach((s) -> System.out.println(s));
        
        System.out.println("Using method references");
        week.stream()
                    .map(String::toUpperCase)
                    .forEach(System.out::println);
    }
    
}
