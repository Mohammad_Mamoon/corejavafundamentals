/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToInstanceMethod;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MathOperation {
    
    public int add(int a ,int b) {
        return a+b;
    }
    
    public int sub(int a ,int b){
        return a-b;
    }
    
    public int multiply(int a , int b) {
        return a*b;
    }
}
