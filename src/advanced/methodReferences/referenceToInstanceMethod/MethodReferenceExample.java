/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToInstanceMethod;

import java.util.function.BiFunction;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MethodReferenceExample {
    public static void main(String[] args) {
        
        MathOperation mp = new MathOperation();
        
        System.out.println("Using lambda expressions");
        BiFunction<Integer,Integer,Integer> add1 = 
            
            
            (a,b) -> { 
                   
                return mp.add(a, b);
                    
            };        
        
        System.out.println("Addition = " + add1.apply(12,121));
        
        BiFunction<Integer,Integer,Integer> sub1 = 
                 (a,b) -> mp.sub(a, b);
        System.out.println("Subtraction = " + sub1.apply(12,121));
        
        BiFunction<Integer,Integer,Integer> mul = 
                (a,b) ->
                        mp.multiply(a, b);
        System.out.println("Multiplication = " + mul.apply(22, 11));
        
        System.out.println("Using method expressions");
        BiFunction<Integer,Integer,Integer> add2 =  mp::add;
        System.out.println("Addition = " + add2.apply(12,121));
        
        BiFunction<Integer,Integer,Integer> sub2 =  mp::sub;
        System.out.println("Addition = " + sub2.apply(12,121));
        
        BiFunction<Integer,Integer,Integer> mult = mp::multiply;
        System.out.println("Multiplication = " + mult.apply(21, 5));
    }
}
