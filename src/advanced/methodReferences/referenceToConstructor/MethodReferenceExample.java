/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences.referenceToConstructor;

import java.util.function.BiConsumer;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MethodReferenceExample {
    public static void main(String[] args) {
        
        System.out.println("using lambda expressions");
        BiConsumer<Integer,Integer> add = (a,b) -> new MathOperations(a,b);
        add.accept(10, 30);
        
        System.out.println("using method references");
        BiConsumer<Integer,Integer> add1 = MathOperations::new;
        add1.accept(10, 30);
    }
}
