/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.methodReferences;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MethodReferenceExample {
    public static void main(String[] args) {
        
        // Using lambda expression
        Predicate p = (n) -> {
            
                 return EvenOddCheck.isEven(n) ;
                   
                   
                   };
            
        System.out.println(p.test(901));
        System.out.println(p.test(30));
        
        
        // Using method reference
        Predicate p1 = EvenOddCheck::isEven;
        System.out.println(p1.test(22));
        
        
        
        
        
        
    }
    
}
