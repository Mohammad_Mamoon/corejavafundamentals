/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StudentPoll {
    public static void main(String[] args) {
        
        int[] responses = { 2,1,4,1,1,5,1,2,3,1,3,1,1,3,4,5,5,3,4,5};
        int[] frequency = new int[6];
        
        for(int i=0; i<responses.length; i++){
            
            if(responses[i]==1)
               frequency[0]+=1;
            else if(responses[i]==2)
                frequency[1]+=1;
            else if(responses[i]==3)
                frequency[2]+=1;
            else if(responses[i]==4)
                frequency[3]+=1;
            else
                frequency[4]+=1;
        }
        
        System.out.printf("%s%10s%n","Rating","Frequency");
        for(int i=1; i<=5; i++) {
            System.out.printf("%6d%10d%n",i,frequency[i-1]);
        }
    }
    
}
