/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.util.function.IntConsumer;
import java.util.stream.IntStream;

/**
 *
 * @author Mohammad_Mamoon
 */
public class IntStreamOperations {
    public static void main(String[] args){
        
        int[] values = { 1,2,3,4,5,6,7,8,9};
        
        // display original values
        System.out.println("Original Values: ");
        
        // here we are passing the int array to the of method
        // which returns IntStream and then we call on forEach method
        // of IntStream and pass it the lambda which implements the
        // accept method of IntConsumer
        IntStream.of(values)
                 .forEach( value -> System.out.printf("%d ", value));
        System.out.println();
        
        
//        IntStream.of(values)
//                 .forEach(new IntConsumer() {
//                 
//                    @Override
//                    public void accept(int a){
//                        System.out.println(a);
//                    }
//                 
//                 }
//                 );
        
        // count , min , max , sum and average of the values
        System.out.printf("%nCount: %d%n",
                IntStream.of(values).count());
        System.out.printf("Min: %d%n",
                IntStream.of(values).min().getAsInt());
        System.out.printf("Sum: %d%n",
                IntStream.of(values).sum());
        System.out.printf("Max: %d%n",
                IntStream.of(values).max().getAsInt()); 
        
        // here average() returns an OptionalDouble containing the average of 
        // all the ints in the stream as a value of double type
        // OptionalDouble works with streams having alteast one element
        // if the stream didn't had any element , then calling the method
        //getAsDouble would throw a NoSuchElementException
        // to prevent from it we can use orElse method which passes the value
        // of OptionalDouble or the value that is passed or otherwise
        System.out.printf("Average: %.2f%n",
                IntStream.of(values).average().getAsDouble());
        
        // alternative way of doing these tasks
        // IntStream provides the summaryStatistics which return 
        // the sum,count,min,max,average in just one pass of an IntStream's
        // elements and returning the result as an IntSummaryStatistics
        // object
        
        System.out.println(IntStream.of(values).summaryStatistics());
        
        // sum of values with the general reduce method
        // reduce takes two arguments
        // first argument begins the reduction operation
        // second argument is the object that implements
        // the IntBinaryOperator interface  through its
        // method applyAsInt
        System.out.printf("Sum: %d%n",IntStream.of(values).reduce(0,(x,y) -> x+y ));
        
        
        // sum of the squares of the elements in values
        System.out.printf("Sum of squares of the elements: %d%n",
                IntStream.of(values).reduce(0,(x,y) -> x + y* y));
        
        // product of the elements in the values
        System.out.printf("Product: %d%n",
                IntStream.of(values).reduce(1, (x,y) -> x * y));
        
        // filtering the elements of values , which is done by implementing the
        // interface the IntPredicate through its method test
        
        System.out.println("Even values displayed in sorted order");
                IntStream.of(values)
                         .filter( value -> value % 2 == 0)
                         .sorted()
                         .forEach( (value) -> System.out.printf("%d ", value)); 
                
        
        // applying map method on the values by multiplying them with 10
        // map implements the IntUnaryOperator interface through its method
        // applyAsInt
        System.out.printf("%nnew elements are : ");
        IntStream.of(values)
                 .filter((value) -> value % 2 != 0)
                 .map( (value) -> value * 10)
                 .sorted()
                 .forEach((value) -> System.out.printf("%d ",value));
        
        System.out.printf("%nsum of intgers from 1 to 9 is: %d ",
        IntStream.rangeClosed(1, 10).sum());
        System.out.println();
        
        System.out.printf("elements from 20-30%n");
        IntStream.rangeClosed(20, 30)
                 .forEach(value -> System.out.printf(" %d",value));
    }
    
}
