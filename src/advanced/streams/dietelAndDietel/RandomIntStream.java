/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.security.SecureRandom;
import java.util.function.Function;
import java.util.stream.Collectors;



/**
 *
 * @author Mohammad_Mamoon
 */
public class RandomIntStream {
    public static void main(String[] args) {
        
        SecureRandom  random = new SecureRandom();
        
        // roll a dice 6,000,000 times and summarise the results
        System.out.printf("%-6s%s%n","Face","Frequency");
        
        // we call the overloaded method ints of SecureRandom class
        // which returns the IntStream containing 6,000,000 random numbers
        // in the range 1-6
        // to summarise the results into a map , we need to convert the IntStream
        // into Stream<Integer> and we do this by calling the boxed method of
        // IntStream
        // then we call the collect method of Stream to summarise the results into
        // a Map<Integer,Long>
        // the first argument to Collectors method groupingBy calls static method
        // identity from the interface Function that simply returns its argument
        // this allows the actual random values  to be used as map's key values
        // the second argument to groupingBy counts the number of occurences of
        // each key
        // for results , we call the forEach method of map which takes the object
        // which implements the BiConsumer functional interface as an argument
        //
        random.ints(6_000_000,1,7)
              .boxed()
              .collect(Collectors.groupingBy(Function.identity(),
                      Collectors.counting()))
              .forEach(
              (face,frequency) -> 
              System.out.printf("%-6d%d%n",face,frequency));
        
    }
}
