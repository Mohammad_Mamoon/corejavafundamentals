/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ProcessingEmployees {
    public static void main(String[] args) {
        
        Employee[] employees = {
            new Employee("Jason","Red",5000,"IT"),
            new Employee("Yasir","Red",3000,"IT"),
            new Employee("Hammad","Green",7000,"Sales"),
            new Employee("Mamoon","Red",4000,"Marketing"),
            new Employee("Nathan","Green",5000,"Sales"),
            new Employee("Kane","Orange",9,"IT")
        };
        
        
        
        List<Employee> list = Arrays.asList(employees);
        System.out.println("Complete list view: ");
        // it creates a stream of Stream<Employee> and then uses the Stream method
        // forEach to display each Employee's string representation
        // the method reference is converted by the compiler into an object which
        // implements the  Consumer interface's accept method
        // accept recieves one argument and returns void
        // here accept passes each Employee's object to the System.out object's
        // instance method println which implicitly calls the toString method of 
        // the Employee
        list.stream()
            .forEach(System.out::println);
        
        // predicate that returns true for salaries between the range 4k - 5k
        Predicate<Employee> fourToFiveThousand = 
                (e) -> (e.getSalary() >= 4000 && e.getSalary() <= 5000);
        
        // display employees in the range 4k - 5k
        // sorted in the ascending order by salary
        // sorted method takes the Comparator as argument
        // we call static method comparing of the Comparator interface
        // which takes the argument that implements the Function interface
        // this function interface is used to extract value
        // from an object in the stream for use in comparisons
        // comparing returns a Comparator which invokes getSalary method
        // on two Employee objects
        
        System.out.printf("%n Employees earning salaries between 4k - 5k%n");
                list.stream()
                    .filter(fourToFiveThousand)
                    .sorted(Comparator.comparing(Employee::getSalary))
                    .forEach(System.out::println);
                
        // Display first employee with salary between 4k - 5k
        // findFirst returns an Optional containing the object that was found
        // get method of Optional  returns the matching object
        System.out.printf("%nFirst Employee with salary between 4k - 5k is %n%s%n",
                  list.stream()
                      .filter(fourToFiveThousand)
                      .findFirst()
                      .get()
                );
        
        // comparing employees based on multiple fields
        // Functions for getting first and last names for an Employee
        Function<Employee,String> byLastName = Employee::getLastName;
        Function<Employee,String> byFirstName = Employee::getFirstName;
        
        Comparator<Employee> lastThenFirst =
                Comparator.comparing(byLastName).thenComparing(byFirstName);
        
        // sort employees by last name and then by first name
        System.out.printf("%nEmployees in ascending order by last name and then"
                + " first:%n");
        list.stream()
            .sorted(lastThenFirst)
            .forEach(System.out::println);    
        
        
        System.out.printf("%nEmployees in descending order by last name and then"
                + " first:%n");
        list.stream()
            .sorted(lastThenFirst.reversed())
            .forEach(System.out::println);
        
        // display unique employee last names sorted
        System.out.printf("%nUnique employee last names:%n");
        list.stream()
            .map(Employee::getLastName)
            .distinct()
            .sorted()
            .forEach(System.out::println);
            
        
        // display only the first and last name
        System.out.printf("%nEmployee names in order by last name and then first name:%n");
        list.stream()
            .sorted(lastThenFirst)
            .map(Employee::getName)
            .forEach(System.out::println);    
        
        
        // group employees according to department
        // here collect method is used to group employees by 
        // department
        // collect takes an argument Collector that specifies how to
        // summarise data into useful form
        // here we use Collector returned by Collectors static method
        // groupingBy
        // groupingBy method recieves a Function  that classifies
        // the objects in the stream - the values returned by this function
        // are used as the keys in a map
        // the corresponding values of the keys are Lists containing
        // the stream elements in a given category
        System.out.printf("%nEmployees by department:%n");
        Map<String,List<Employee>> groupByDepartment =
             list.stream()
                 .collect(Collectors.groupingBy(Employee::getDepartment));
        
        
        // here map's method forEach performs an operation on each key-value
        // pairs.
        // The argument to the method forEach  is an object that implements
        // BiConsumer functional Interface through its method accept
        // which takes two arguments
        // for maps the first one is for key
        // second one for the value
        groupByDepartment.forEach(
                (Department,employeesInDepartment) -> {
                    System.out.println(Department);
                    employeesInDepartment.forEach(
                    employee -> System.out.printf(" %s%n",employee));
                    
                }
        
        );
        
        // count number of employees in each department
        
        System.out.printf("%nCount of employees by department%n");
        
        // here we use the collect method of stream which takes the
        // argument of Collector interface implementation that is Collectors
        // class through its static method groupingBy
        // grouping by takes two arguments
        // ist is function that classifies objects
        // second is again a Collectors class through its static method
        // counting that returns the number of objects falling in each
        // category
        Map<String,Long> employeeCountByDepartment =
            list.stream()
                .collect(Collectors.groupingBy(Employee::getDepartment,
                         Collectors.counting()));
                        
        
        employeeCountByDepartment.forEach(
         (department,count) ->
                 System.out.printf("%s has %d employee(s)%n",department,count)
        
        );
        
        // here we are using the Stream's method mapToDouble which maps an
        // objects to double values and return a DoubleStream.
        // method mapToDouble recieves an object that implements
        // the functional interface ToDoubleFunction through its method
        // applyAsDouble 
        // this method invokes an instance method on an object an returns
        // a double value
        
        // sum of Employee salaries with DoubleStream sum method
        System.out.printf("%nSum of Employees salaries (via sum method): %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .sum());
        
        
        // sum of Employee salaries with Stream reduce method
        System.out.printf("%nSum of Employees salaries (via sum method): %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .reduce(0,(value1,value2) -> value1 + value2));
        
        
        // Average of Employee salaries with DoubleStream average method
        System.out.printf("%nSum of Employees salaries (via sum method): %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .average()
                    .getAsDouble());
    }
}
