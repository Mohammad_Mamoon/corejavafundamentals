/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArraysAndStreams2 {
    public static void main(String[] args){
        
        String[] strings = {"Red","blue","green","orange","Cyan"};
        
        System.out.printf("Original Values: %s%n",Arrays.asList(strings));
        
        // map expects an object of the class implementing the functional
        // interface Function through its
        // method apply , the lambda expression is treated as the implemen
        // tation
        
        // here we are invoking the instance method toUpperCase of the
        // String class , its a method reference
        
        // String::toUpperCase is a shorthand notation for the lambda
        // (String s) -> ( return s.toUpperCase();)
        
        System.out.printf("objects in UpperCase: %s%n",
                Arrays.stream(strings)
                      .map(String::toUpperCase)
                      .collect(Collectors.toList()));
        
        // strings greater than "m"(case insensitive) sorted ascending
        // sorted method takes Comparator as an argument
        // and for that we are passing predefined Comparator
        // String.CASE_INSENSITIVE_ORDER
        System.out.printf("strings greater than m sorted ascending: %s%n",
                Arrays.stream(strings)
                      .filter(s -> s.compareToIgnoreCase("m") > 0)
                      .sorted(String.CASE_INSENSITIVE_ORDER)
                      .collect(Collectors.toList()));
        
        System.out.printf("strings greater than m sorted descending: %s%n",
                Arrays.stream(strings)
                      .filter(s -> s.compareToIgnoreCase("m") > 0)
                      .sorted(String.CASE_INSENSITIVE_ORDER.reversed())
                      .collect(Collectors.toList()));

     
    }
    
}
