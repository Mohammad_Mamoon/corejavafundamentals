/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArraysAndStreams {
    
    public static void main(String[] args){
        Integer[] values = { 1,2,3,4,10,6,7,8};
        
        // display the original values
        System.out.printf("Original values: %s%n",Arrays.asList(values));
        
        
        // to create a collection we are using the collect method
        // which recieves an object that implements the interface
        // collector
        // here class Collectors provide the static methods that return
        // predefined collector implementations
        
        // Stream<Integer> is transformed to List<Integer>
        // List<Stream> is displayed with an implicit call to toString
        System.out.printf("Sorted values: %s%n",
                Arrays.stream(values)
                      .sorted()
                      .collect(Collectors.toList())); 
        
        
        List<Integer> greaterThan4 =
                Arrays.stream(values)
                      .filter((value) -> value > 4)
                      .sorted() 
                      .collect(Collectors.toList());
        
        System.out.println(greaterThan4);
        
        List<Integer> greaterThan5 =
                Arrays.stream(values)
                      .filter((value) -> value >5)
                      .sorted()
                      .collect(Collectors.toList());
        
        System.out.println(greaterThan5);
    }
    
}
