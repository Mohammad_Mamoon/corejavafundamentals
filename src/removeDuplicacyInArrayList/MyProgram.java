/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package removeDuplicacyInArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Vector;


/**
 *
 * @author Mohammad_Mamoon
 */
public class MyProgram {
    public static void main(String[] args) {
        
        // Iterable interface sits at the top of the tree and
        // is the root interface for all the collection classes
        
        // the Collection interface extends the Iterable interface
        // and therefore all the subclasses of Collection interface
        // also implement the Iterable interface
        // it contains only one abstract method 
        // iterator() which returns an Iterator over the elements of type T
        
        List<Integer> al = new ArrayList<>();
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(3);
        al.add(4);
     
        // this line uses iterator method of the Iterable interface
        // which returns an Iterator 
        Iterator iterator = al.iterator();
        
        while(iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        
        System.out.println();
        
       
        
        
        List<Integer> vector = new Vector<>();
        vector.add(5);
        vector.add(21);
        vector.add(311);
        vector.add(36);
        vector.add(4);
     
        Iterator iterator1 = vector.iterator();
        
        while(iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }
        
        System.out.println();
        
        Stack<Integer> stack = new Stack<>();
        stack.push(15);
        stack.push(13);
        stack.push(31);
        stack.push(66);
        stack.push(55);
     
        Iterator iterator2 = stack.iterator();
        
        while(iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        
        System.out.println("");
        
        PriorityQueue<String> pq = new PriorityQueue();
        pq.add("mamoon");
        pq.add("nasir");
        pq.add("amir");
        pq.add("Yusra");
        
        System.out.println(pq.element());
        System.out.println(pq.remove());
        System.out.println(pq.poll());
     
   
    }
    
}
