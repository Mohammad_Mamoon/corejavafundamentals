/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdaExample;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassC {
    public static void main(String[] args) {
        //ClassB obj = new ClassB();
        //obj.message();
        
//        InterfaceA obj = new InterfaceA() {
//            
//            @Override
//            public void message() {
//            System.out.println("hi my friend");
//        
//            }
//            
//        };
//        
//        obj.message();


        InterfaceA obj = () -> {
            System.out.println("lambda way");
        };
        
        obj.message();
        
        InterfaceD obj2 = (a,b) ->  a + b;
      
        
        int sum = obj2.add(3, 4);
        System.out.println(sum);
        
    }
    
}
