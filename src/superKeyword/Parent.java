/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superKeyword;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Parent {
    
    protected int num = 10;
    
    Parent(){
        System.out.println("Superclass contructor");
    }
    
    public void message() {
        System.out.println("message from parent class");
    }
    
}
