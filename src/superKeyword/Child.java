/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superKeyword;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Child extends Parent {
    
    int num = 20;
    
    Child() {
        
        // used to call the constructor of the super class
        super();
        System.out.println("Subclass constructor");
    }
    
    public void message() {
        System.out.println("message from child class");
    }
    
    public void method() {
        Child sub = new Child();
        sub.message();
        
        // used to call the members of the superclass with the same name as 
        // that of the subclass members
        super.message();
        
        System.out.println("value of num in subclass " + sub.num);
        
        System.out.println("value of num in superclass " + super.num);
        
    }
    
    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.method();
    }
    
}
