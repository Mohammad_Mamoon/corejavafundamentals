/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.genericStack;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StackTest {

    public static void main(String[] args) {
        double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
        int[] integerElements = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        Stack<Double> doubleStack = new Stack(5);
        Stack<Integer> integerStack = new Stack();

        testPushDouble(doubleStack, doubleElements);
        testPopDouble(doubleStack);

        testPushInteger(integerStack, integerElements);
        testPopInteger(integerStack);

    }

    private static void testPushDouble(Stack<Double> stack, double[] values) {

        System.out.println("Pushing elements onto the doubleStack");

        for (double value : values) {
            System.out.printf("%.1f ", value);
            stack.push(value);

        }
        System.out.println();
    }

    private static void testPopDouble(Stack<Double> stack) {

        try {
            System.out.println("Poping elements from the doubleStack");
            double popValue;

            while (true) {
                popValue = stack.pop();
                System.out.printf("%.1f ", popValue);
            }
        } catch (EmptyStackException e) {
            System.err.println();
            e.printStackTrace();
        }
    }

    private static void testPushInteger(Stack<Integer> stack, int[] values) {
        System.out.println();
        System.out.println("Pushing elements onto the integerStack");

        for (int value : values) {
            System.out.printf("%d ", value);
            stack.push(value);
        }
        System.out.println();
    }

    private static void testPopInteger(Stack<Integer> stack) {

        try {
            System.out.println("Poping elements from the Integerstack");
            System.out.println();
            int popValue;

            while (true) {
                popValue = stack.pop();
                System.out.printf("%d ", popValue);
            }
        } catch (EmptyStackException e) {
            System.err.println();
            e.printStackTrace();
        }
    }
}


     