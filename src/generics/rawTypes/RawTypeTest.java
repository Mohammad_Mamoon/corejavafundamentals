/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.rawTypes;

/**
 *
 * @author Mohammad_Mamoon
 */
public class RawTypeTest {
    public static void main(String[] args) {
        Integer[] integerElements = {1,2,3,4,5,6,7,8,9,10};
        Double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
        
        
        // Stack of raw types assigned to stack of raw types variable
        Stack rawTypeStack1 = new Stack(5);
        
        // stack of double type assigned to stack of raw types variable
        Stack rawTypeStack2 = new Stack<Double>(5);
        
        // Stack of raw type assigned to stack of integer type variable
        Stack<Integer> integerStack = new Stack(10);
        
        
         testPush("rawTypeStack1", rawTypeStack1, doubleElements);
         testPop("rawTypeStack1", rawTypeStack1); 
         testPush("rawTypeStack2", rawTypeStack2, doubleElements); 
         testPop("rawTypeStack2", rawTypeStack2); 
         testPush("integerStack", integerStack, integerElements); 
         testPop("integerStack", integerStack); 
    }
    
    
    // generic method pushes elements onto stack 
    public static <T> void testPush(String name, Stack<T> stack, T[] elements){
        System.out.println();
        System.out.printf("Pushing elements onto %s", name);
        System.out.println();
         
        // push elements onto Stack           
        for (T element : elements){ 
        
            System.out.printf("%s ", element);
            stack.push(element); 
            
        }    
    }     

    // generic method testPop pops elements from stack 
    public static <T> void testPop(String name, Stack<T> stack){ 
        // pop elements from stack    
        try {   
            System.out.println();
            System.out.printf("Popping elements from %s", name);
            System.out.println();
            T popValue; 
            
            // store element removed from stack 
            // remove elements from Stack 
            while (true){
            
            popValue = stack.pop();
            System.out.printf("%s ", popValue); 
            
            }
        
        }catch(EmptyStackException emptyStackException){ 
            System.out.println(); 
            emptyStackException.printStackTrace(); 
        }  
    }
}
