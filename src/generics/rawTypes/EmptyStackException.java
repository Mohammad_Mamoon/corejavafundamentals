/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.rawTypes;

/**
 *
 * @author Mohammad_Mamoon
 */
public class EmptyStackException extends RuntimeException {
    public EmptyStackException(){
        this("This stack is empty");
    }
    
    public EmptyStackException(String message) {
        super(message);
    }
    
}
