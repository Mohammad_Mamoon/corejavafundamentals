/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.introWildcard;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TestNumbers {
    public static void main(String[] args){
        Number[] numbers = {1, 2.2, 3, 4.4};
        ArrayList<Number> al = new ArrayList();
        
        for(Number element : numbers) {
            al.add(element);
        }
        
        System.out.println("ArrayList elements are: " + al);
        System.out.printf("sum of the elements of the ArrayList is %.1f ", sum(al));
        
    }
    
    public static double sum(ArrayList<Number> al){
        
        double total = 0.0;
        for(Number element : al){
            total += element.doubleValue();
        }
        return total;
    }
    
}
