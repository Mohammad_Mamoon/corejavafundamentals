/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.introWildcard;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TestNumbers2 {
    public static void main(String[] args) {
        Integer[] integerElements = {1, 2, 3, 4};
        ArrayList<Integer> al = new ArrayList();
        
        for(Integer element: integerElements)
            al.add(element);
        
        System.out.println("ArrayList elements are : " + al);
        System.out.println("sum of the elements is: " + sum(al));
        
        
        Double[] doubleElements = { 1.1, 2.2, 3.3, 4.4, 5.5 };
        ArrayList<Double> aList = new ArrayList();
        
        for(Double element: doubleElements)
            aList.add(element);
        
        System.out.println("ArrayList elements are : " + aList);
        System.out.println("sum of the elements is: " + sum(aList));
    }
    
    public static double sum(ArrayList< ? extends Number> al){
        double total = 0.0;
        
        for(Number element : al)
            total += element.doubleValue();
        
        return total;
    } 
    
}
