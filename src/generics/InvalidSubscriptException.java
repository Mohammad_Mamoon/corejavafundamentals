/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class InvalidSubscriptException extends Exception {
    
    public  InvalidSubscriptException(String message) {
        super(message);
    }
    
}
