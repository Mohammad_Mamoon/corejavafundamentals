/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.genericClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    public static void main(String[] args) {
         
        Integer[] arr = { 1,2,3 };
        Character[] arr1 = { 'w','c','f' };
        
        ClassA obj = new ClassA();
        obj.display(arr);
        System.out.println();
        obj.display(arr1);
        
        
    }
    
}
