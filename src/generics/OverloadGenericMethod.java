/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class OverloadGenericMethod {
     public static void main(String[] args) throws InvalidSubscriptException  { 
     // create arrays of Integer, Double and Character 
     Integer[] integerArray = {1, 2, 3, 4, 5, 6}; 
     Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
     Character[] characterArray = {'H', 'E', 'L', 'L', 'O'};
     
     Scanner input = new Scanner(System.in);
     
      System.out.printf("Array integerArray contains:%n");  
      printArray(integerArray);
      System.out.println("Printing a portion of the Integer array between (0-5)");
      System.out.println("Which portion you want to print");
      System.out.println("lower range ?");
      int low = input.nextInt();
      System.out.println("upper range ?");
      int upper = input.nextInt();
      
      if((low< 0)  || (upper > 5)) {
          throw new InvalidSubscriptException("improper range");
      }
      
      else {
          printArray(integerArray,low,upper);
      }
         
      
      System.out.printf("%nArray doubleArray contains:%n");   
      printArray(doubleArray);
      System.out.println("Printing a portion of the Double array");
      printArray(doubleArray,2,5);
      System.out.printf("%nArray characterArray contains:%n");  
      printArray(characterArray);
      System.out.println("Printing a portion of the Character array");
      printArray(characterArray,2,4);
     
     }
     
     // generic method , to print elements of any type
     public static <T> void printArray(T[] inputArray){
         
      // display array elements 
      for (T element : inputArray) 
          System.out.printf("%s ", element); 
         
      System.out.println(); 
      
     }
     
     // generic method , to print portion of array
     public static <T> void printArray(T[] inputArray,int lowSubscript,int highSubscript){
         
      // display array elements 
      for(int i=lowSubscript; i < highSubscript ; i++ ){ 
          System.out.printf("%s ", inputArray[i]); 
          
      }   
      
      System.out.println(); 
      
     }
     
     
    
    
}
