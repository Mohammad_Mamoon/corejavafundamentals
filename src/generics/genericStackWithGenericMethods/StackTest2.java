/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics.genericStackWithGenericMethods;


/**
 *
 * @author Mohammad_Mamoon
 */
public class StackTest2 {
    public static void main(String[] args) {
        Double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5 };
        Integer[] integerElements= {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        Stack<Double> doubleStack = new Stack<>(5);
        Stack<Integer> integerStack = new Stack<>();
        
        testPush("doubleStack",doubleStack,doubleElements);
        testPop("doubleStack",doubleStack);
        
        testPush("integerStack",integerStack,integerElements);
        testPop("integerStack",integerStack);
        
        
    }
    
    public static <T> void testPush(String name,Stack<T> stack, T[] values ){
        
        System.out.printf("%nPushing elements onto %s%n",name);
        
        for(T value : values){
            System.out.printf("%s ",value);
            stack.push(value);
        }
    }
    
    public static <T> void testPop(String name,Stack<T> stack){
        
        try{
            System.out.printf("%nPoping elements from the %s%n",name);
            T popValue;
            
            while(true){
                popValue = stack.pop();
                System.out.printf("%s ",popValue);
            }
        }catch(EmptyStackException e){
            System.err.println();
            e.printStackTrace();
        }
    }
    
}
