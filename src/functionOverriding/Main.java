/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionOverriding;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main {
    public static void main(String[] args) {
        
        Parent obj = new Parent();
        Parent obj2 = new Child();
        
        obj.message();
        obj2.message();
        //obj2.greet();
    }
    
}
