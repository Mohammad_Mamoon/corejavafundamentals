/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionOverriding;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Child extends Parent {
    
    public void message() {
        
        // invoking the superclass version of the overridden method
        super.message(); 
        System.out.println("Its a message from the child class");
    }
    
    public void greet() {
        System.out.println("hello there");
    }
}
