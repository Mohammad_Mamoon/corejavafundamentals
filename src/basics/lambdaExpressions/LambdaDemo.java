/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.lambdaExpressions;

/**
 *
 * @author Mohammad_Mamoon
 */
public class LambdaDemo {
    public static void main(String[] args) {
        Runnable r =  () ->  { 
             
        
         
            System.out.println("Implementing run method through the lambdas");
        
            
        };
        
        r.run();


            // alternative to this

//          Runnable r = new Runnable() {
//            
//              @Override
//              public void run() {
//                  System.out.println("Running");
//              }
//          };
//          
//          r.run();
    
    }
    
}
