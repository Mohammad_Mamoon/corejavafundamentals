/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.lambdaExpressions;

/**
 *
 * @author Mohammad_Mamoon
 */
public class AnotherExample {
    public static void main(String[] args) {
        Thread t = new Thread( new Runnable() {
            
            public void run() {
                System.out.println("run method 1");
        
        }}); 
    
        t.start();
        
    
        Thread t1 = new Thread( () -> {
        
                System.out.println("implementing run method of Runnable functional interface");
                System.out.println("run method 2");
        });
        
        t1.start();
        
        
        
        Thread t2 = new Thread( () ->
                 AnotherExample.threadStatus()
        );
           
        t2.start();
        
        
        Thread t3 = new Thread(AnotherExample::threadStatus);
        t3.start();
    }
    
    public static void threadStatus(){
        System.out.println("Thread is running");
    }
    
}
