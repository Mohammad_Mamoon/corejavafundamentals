/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.queueImplementations;

import java.util.Iterator;
import java.util.PriorityQueue;

/**
 *
 * @author Mohammad_Mamoon
 */
public class PriorityQueueExample1 {
    
    public static void main(String[] args) {
        
        PriorityQueue<String> queue = new PriorityQueue();
        
        queue.add("Hawal");
        queue.add("Naseem Bagh");
        queue.add("japan");
        queue.add("Canada");
        
        // PriorityQueue doesn't allow nulls
        //queue.add(null);
        
        System.out.println("---------");
        
        System.out.println("head: " + queue.element());
        System.out.println("head: " + queue.peek());
        
        System.out.println("------------");
        
        // iterating the queue elements
        Iterator itr = queue.iterator();
        while(itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        queue.remove();
        queue.poll();
        
        System.out.println("--------");

        // after removing two elements
        Iterator itr2 = queue.iterator();
        while(itr2.hasNext()) {
            System.out.println(itr2.next());
        }
        
        
    }

    
}
