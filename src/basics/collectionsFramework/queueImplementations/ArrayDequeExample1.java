/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.queueImplementations;

import java.util.ArrayDeque;
import java.util.Iterator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayDequeExample1 {
    
    public static void main(String[] args) {
        
        //creating ArrayDeque
        ArrayDeque<String> deque = new ArrayDeque();
        
        //adding objects in ArrayList
        deque.add("Srinagar");
        deque.add("paris");
        deque.add("Srinagar");
        deque.add("florida");
        
        //deque.add(null); // not allowed
        
        
        //Traversing list through Iterator
        Iterator iterator = deque.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        System.out.println("--------");
        
        deque.offer("malaysia");
        deque.offer("thailand");
        
        deque.offerFirst("vietnam");
        deque.offerLast("kerala");
        
        for(String s : deque)
            System.out.println(s);
        
        System.out.println("---------------");
        
        deque.poll();
        deque.pollFirst();
        deque.pollLast();
        
        for(String s : deque)
            System.out.println(s);
        
    }
}
