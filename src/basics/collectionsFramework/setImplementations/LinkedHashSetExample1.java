/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.setImplementations;

import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 *
 * @author Mohammad_Mamoon
 */
public class LinkedHashSetExample1 {
    
    public static void main(String[] args) {
        
        //creating HashSet
        LinkedHashSet<String> set = new LinkedHashSet();
        
        //adding objects in HashSet
        set.add("Srinagar");
        set.add("paris");
        set.add("Srinagar");
        set.add("florida");
        set.add(null);
        set.add(null);
       
        
        //Traversing list through Iterator
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
