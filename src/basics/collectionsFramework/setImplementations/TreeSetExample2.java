/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.setImplementations;

import java.util.TreeSet;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TreeSetExample2 {
    
    public static void main(String[] args) {
        
        
        // Elements in the TreeSet must be of Comparable type
        // String and Wrapper classes are Comparable by default
        // so adding user-defined objects in the TreeSet, we need to
        // implement the Comparable interface
        
        TreeSet<Book> set = new TreeSet();
        set.add(new Book(121,"Let us C","Yk","BPB",8));
        set.add(new Book(233,"Operating System","Galvin","Wiley",6));
        set.add(new Book(101,"C++","abc","Arihant",12));
        
        for(Book b : set) {
            System.out.println(b.getId() + " " + b.getName() +
                    " " + b.getPublisher() + " " + b.getQuantity());
        }
        
    }
    
}

class Book implements Comparable<Book> {
    
    private int id;
    private String name, author, publisher;
    private int quantity;

    public Book(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(Book obj2) {
        
        if(this.getId() > obj2.getId())
            return 1;
        
        else if(this.getId() < obj2.getId())
            return -1;
        
        else
            return 0;
    }
    
    
}
