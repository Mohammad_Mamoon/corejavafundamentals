/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.setImplementations;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class HashSetExample1 {
    
    public static void main(String[] args) {
        
        //creating HashSet
        HashSet<String> set = new HashSet();
        
        //adding objects in HashSet
        set.add("Srinagar");
        set.add("paris");
        set.add("Srinagar");
        set.add("florida");
        set.add(null);
        set.add(null);
       
        
        //Traversing list through Iterator
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        //removing specific element from the set
        set.remove("paris");
        System.out.println(set);
        
        HashSet<String> set2 = new HashSet();
        set2.add("japan");
        set2.add("london");
        System.out.println(set2);
        
        set.addAll(set2);
        System.out.println(set);
        
        set.removeAll(set2);
        System.out.println(set);
        
        set.clear();
        System.out.println(set);
        
        
    }
    
}
