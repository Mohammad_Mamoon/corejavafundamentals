/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionsFramework.setImplementations;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TreeSetExample {
    
    public static void main(String[] args) {
        
        //creating HashSet
        TreeSet<String> set = new TreeSet();
        
        //adding objects in TreeSet which stores the objects in the 
        // ascending order
        set.add("Srinagar");
        set.add("paris");
        set.add("Srinagar");
        set.add("florida");
        set.add("pak");
        set.add("japan");
       // set.add(null);  // doesnt allow null values
        
       
        
        //Traversing list through Iterator
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        
        System.out.println("---------");
        
        Iterator itr = set.descendingIterator();
        while(itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        System.out.println("------");
        
        System.out.println(set.pollFirst());
        System.out.println(set.pollLast());
       
        System.out.println("--------");
        
        // performing NavigableSet operations
        
        System.out.println("Initial set: " + set);
        
        System.out.println("Reverse set: " + set.descendingSet());
        
        System.out.println("Head set: " + set.headSet("pak",true));
        
        System.out.println("SubSet: " + set.subSet("Srinagar", false, "pak", true));
        
        System.out.println("TailSet: " + set.tailSet("florida", false));
        
        
        System.out.println("--------");
        
        // SortedSet operations
        
        System.out.println("Head set: " + set.headSet("pak"));
         
        System.out.println("SubSet: " + set.subSet("Srinagar","pak"));
        
        System.out.println("TailSet: " + set.tailSet("florida"));
        
        
    }
    
}
