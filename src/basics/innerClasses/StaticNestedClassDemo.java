/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StaticNestedClassDemo {
    public static void main(String[] args) {
        
        // accessing a static nested class
        OuterClass.StaticNestedClass obj =  new OuterClass.StaticNestedClass();
        obj.display();
    }
}
    

