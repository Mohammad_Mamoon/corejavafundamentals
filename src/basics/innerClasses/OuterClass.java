/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses;

/**
 *
 * @author Mohammad_Mamoon
 */
public class OuterClass {
    
    // static member
    static int outer_x = 10;
    
    // instance(non-static) member
    int outer_y = 20;
    
    // private member
    private static int outer_private = 30;
    
    // static nested class
    static class StaticNestedClass {
        void display() {
            
            // can access static member of outer class
            System.out.println("Outer_x = " + outer_x);
            
            // can access private static member of outer class
            System.out.println("outer_private = " + outer_private);
            
            // the following statement will give compilation error
            // as static nested class cannot access the non-static
            // fields
            //System.out.println("outer_y = " + outer_y);
        }
    }
    
}


