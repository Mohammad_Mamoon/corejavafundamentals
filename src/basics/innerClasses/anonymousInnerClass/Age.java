/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses.anonymousInnerClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public interface Age {
    
    static final int x = 21;
    
    void getAge();
    
}
