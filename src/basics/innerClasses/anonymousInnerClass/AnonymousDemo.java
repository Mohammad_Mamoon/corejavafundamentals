/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses.anonymousInnerClass;

import static basics.innerClasses.anonymousInnerClass.Age.x;

/**
 *
 * @author Mohammad_Mamoon
 */
public class AnonymousDemo {
    public static void main(String[] args) {
        Age obj = new Age() {
            
          @Override  
          public void getAge(){
              System.out.println("my age is = " + x);
          }
            
        };
        
        obj.getAge();
    }
    
}

     // alternative to the below way

//public class AnonymousDemo implements Age {
//    public static void main(String[] args) {
//        
//         AnonymousDemo obj = new AnonymousDemo();
//         obj.getAge();
//    }
//    
//    @Override  
//          public void getAge(){
//              System.out.println("my age is = " + x);
//          }
//    
//}
