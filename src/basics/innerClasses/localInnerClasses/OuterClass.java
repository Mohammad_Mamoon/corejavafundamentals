/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses.localInnerClasses;

/**
 *
 * @author Mohammad_Mamoon
 */
public class OuterClass {
    
     // static member
     static int outer_x = 10;
     
     // instance(non-static) member
     int outer_y = 20;
     
     // priavte member
     private int outer_private = 30;
     
     // inner class
    public class InnerClass {
         void display() {
             
             // can access static member of outer class
             System.out.println("Outer_x = " + outer_x);
             
             // can access also non-static fiels
             System.out.println("Outer_y = " + outer_y);
             
             // can also access private fields
             System.out.println("Outer_private = " + outer_private);
         }
     }
    
}
