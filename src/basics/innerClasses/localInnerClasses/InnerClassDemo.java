/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.innerClasses.localInnerClasses;

/**
 *
 * @author Mohammad_Mamoon
 */
public class InnerClassDemo {
    public static void main(String[] args) {
        
        // accessing an inner class
        OuterClass obj1 = new OuterClass();
        OuterClass.InnerClass obj2 = obj1.new InnerClass();
        obj2.display();
    }
    
}
