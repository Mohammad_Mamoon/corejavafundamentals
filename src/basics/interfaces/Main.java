/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.interfaces;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main implements Greet, FirstInterface  {
    
    public static void main(String[] args) {
        
        Main obj = new Main();
        obj.message();
        obj.display();
        //obj.call();
        FirstInterface.say();
        
        System.out.println(num);
        
        
    }
    
    @Override
    public void message() {
        System.out.println("Hello World");
    }

    @Override
    public void display() {
        Greet.super.display(); 
        //System.out.println("A new message");
    }

    @Override
    public void call() {
        System.out.println("Calling all autobots");
    }
    
    
}
