/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.interfaces;

/**
 *
 * @author Mohammad_Mamoon
 */
public interface FirstInterface {
    
    // static methods java 8 onwards and can be called independently of the object
    static void say() {
        System.out.println("hello there");
    }
    public void call();
    //public void say();
    
}
