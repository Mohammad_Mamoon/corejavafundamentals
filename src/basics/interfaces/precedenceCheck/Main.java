/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.interfaces.precedenceCheck;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main extends Student implements Interface1 , Interface2  {
    
    public static void main(String[] args) {
        Student obj = new Main();
        obj.info();
    }
    
    public void info() {
        Interface2.super.info();
   }
//    
//    public void info() {
//        Interface1.super.info();
//   }
    
//    public void info() {
//        System.out.println("overriden");
//   }
    
}
