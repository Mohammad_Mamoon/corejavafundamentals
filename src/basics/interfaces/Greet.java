/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.interfaces;

/**
 *
 * @author Mohammad_Mamoon
 */
public interface Greet {
    
    // variables are public,final and static
    int num = 10;
    
    void message();
    
    // default method
    default void display() {
        System.out.println("A new addition");
    }
    
}
