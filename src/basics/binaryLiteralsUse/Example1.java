/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.binaryLiteralsUse;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Example1 {
    
    public static void main(String[] args) {
        
        byte num1 = 5;
        byte num2 = 0B101;
        
        System.out.println("Is num1 == num2: " + (num1==num2));
        
        short num3 = -0b001;
        System.out.println("value of num3: " + num3);
        
        int  num4 = 0B10_01;
        System.out.println("Value of num4: " + num4);
    }
    
}
