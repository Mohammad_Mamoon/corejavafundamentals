/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.polymorphism.payrollExample;

/**
 *
 * @author Mohammad_Mamoon
 */
public class BasePlusCommissionEmployee extends CommissionEmployee {
    
    private double baseSalary;  // base salary per week
    
    // six-argument constructor
    public BasePlusCommissionEmployee(String firstName, String lastName,
            String socialSecurityNumber, double grossSales,
            double commissionRate, double baseSalary) {
        
        // explicit call to superclass's constructor
        super(firstName,lastName,socialSecurityNumber,grossSales,
                commissionRate);
        
         // if baseSalary is invalid throw exception
        if(baseSalary < 0.0)
            throw new IllegalArgumentException(
                    "Base Salary must be >= 0.0");
        
        this.baseSalary = baseSalary;
        
    }
    
    
     public double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        
        if(baseSalary < 0.0)
            throw new IllegalArgumentException(
                    "Base Salary must be >= 0.0");
        
        this.baseSalary = baseSalary;
    }
    
    
    @Override
    public double earnings() {
        
        return getBaseSalary() + super.earnings(); 
    }
    
    // return String representation of BasePlusCommissionEmployee
    @Override
    public String toString() {
        return String.format("%s %s; %s: $%,.2f",
                "base-salaried",super.toString(),
                "base salary",getBaseSalary());
        
    }
}
