/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.files.basics;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ReadTextFile {
    
    private static Scanner input;
    
    public static void main(String[] args) {
        
        openFile();
        readRecords();
        closeFile();
    }
    
    
    // open text file
    public static void openFile(){
        
        try{
            input = new Scanner(Paths.get("D:\\Directory\\clients.txt"));
        }catch(IOException ioException){
            System.err.println("Error opening file, terminating");
            System.exit(1);
        }
        
    }
    
    
    // read records from a file
    public static void readRecords(){
        System.out.printf("%-10s%-12s%-12s%10s%n", "Account","First Name","Last Name","Balance");
        
        try{
            
            while(input.hasNext()){
                
                // display record contents
                System.out.printf("%-10s%-12s%-12s%10.2f%n",input.nextInt()
                ,input.next(),input.next(),input.nextDouble());
            }
        }catch(NoSuchElementException elementException){
            System.err.printf("File improperly formed, terminating");   
        }catch(IllegalStateException stateException){
            System.err.println("Error reading file,terminating");
        }
    }
    
    // close file
    public static void closeFile(){
        if(input != null)
            input.close();
    }
    
}
