/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.files.basics;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ReadSequentialFile {
    
    private static ObjectInputStream input;
    
    public static void main(String[] args){
        
        openFile();
        readRecords();
        closeFile();
    }
    
    
    public static void openFile(){
        try{
        input = new ObjectInputStream(Files.newInputStream(
                Paths.get("D:\\Directory\\clients.ser")));
        }catch(IOException ioException){
            System.err.println("Error openinf file, terminating");
            System.exit(1);
        }
    }
    
    public static void readRecords(){
        System.out.printf("%-10s%-12s%-12s%10s%n", 
                "Account","First Name","Last Name","Balance");
        
        try{
            
            while(true){
                Account record = (Account) input.readObject();
                
                // display record contents
                System.out.printf("%-10s%-12s%-12s%10.2f%n",
                        record.getAccount(),record.getFirstName(),
                        record.getLastName(),record.getBalance());
            }
        }catch(EOFException endOfFileException){
            System.out.printf("%n No more Records %n");
            
        }catch(ClassNotFoundException classnotfoundexception ){
            System.err.println("Invalid object type , terminating");
        }catch(IOException ioException){
            System.err.printf("Error reading from file, terminating");
        }
    }
    
    
    public static void closeFile(){
        try{
            if(input != null)
                input.close();
        }catch(IOException ioException){
            System.err.println("Error closing file, terminating");
            System.exit(1);
        }
    }
    
}
