/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.files.basics;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class CreateSequentialFile{ 
        private static ObjectOutputStream output;
        
        public static void main(String[] args){
            
            openFile();
            addRecords();
            closeFile();
        }
        
        public static void openFile(){
            
            try{
                output = new ObjectOutputStream(Files.newOutputStream
                                (Paths.get("D:\\Directory\\clients.ser")));
            }catch(IOException ioException){
                System.err.println("Error opening file,terminating");
                System.exit(1);
            }
        }
        
        
        public static void addRecords(){
            
            char ch = 'y';
            Scanner input = new Scanner(System.in);
            
            System.out.printf("%s%n%s%n",
                    "Enter account number,First Name,Last Name and Balance",
                    "Enter end-of-file indicator to end input");
            
            do{
                
                try{
                    Account record = new Account(input.nextInt(),input.next(),
                    input.next(),input.nextDouble());
                    
                    // serialize record object to a file
                    output.writeObject(record);
                    System.out.println("Do you want to continue");
                    ch = input.next().charAt(0);
                }catch(NoSuchElementException elementException){
                    System.err.println("Invalid input,Please try again");
                    System.exit(1);
                }catch(IOException ioException){
                    System.err.println("Error writing to file,terminating");
                    break;
                }
                
                System.out.print("? "); 
            }while(ch == 'y' || ch == 'Y');
            
            
        }
        
        
        public static void closeFile() {
        try {
            if (output != null) {
                output.close();
            }
        } catch (IOException ioException) {
            System.out.printf("Error closing file ,terminating");
        }
    }
}
