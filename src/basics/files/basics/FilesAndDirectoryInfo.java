/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.files.basics;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class FilesAndDirectoryInfo {
    public static void main(String[] args) throws IOException{
        
        Scanner input = new Scanner(System.in);
        System.out.println("Enter file or directory name: ");
        
        // create path object based on user input
        Path path = Paths.get(input.nextLine());
        
        // if path exists output info about it
        if(Files.exists(path)) {
            
            // display file (or directory) information
            System.out.printf("%n%s exists %n",path.getFileName());
            System.out.printf("%s a directory %n",Files.isDirectory(path) ? "Is" : "Is Not");
            System.out.printf("%s an absolute path %n",path.isAbsolute() ? "Is" : "Is not");
            System.out.printf("Last modified : %s%n",Files.getLastModifiedTime(path));
            System.out.printf("Size : %s%n",Files.size(path));
            System.out.printf("Path : %s%n ", path);
            System.out.printf("Absolute path : %s",path.toAbsolutePath());
            
            // output directory listing
            if(Files.isDirectory(path)){
                System.out.printf("%n Directory Contents %n");
                
                // object for iterating through the directory's contents
                DirectoryStream<Path> ds = Files.newDirectoryStream(path);
                
                for(Path p : ds){
                    System.out.println(p);
                }
            }
            
            
        }
        
        // not file or directory , output error message
        else {
            System.out.printf("%s does not exist %n", path);
        }
        
    }
    
}
