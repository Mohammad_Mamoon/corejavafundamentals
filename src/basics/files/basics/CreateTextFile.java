/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.files.basics;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class CreateTextFile {
    private static Formatter output;
    
    public static void main(String[] args) {
        
        openFile();
        addRecords();
        closeFile();
    }
    
    // open file clients.txt
    public static void openFile(){
        
        try{
            output = new Formatter("D:\\Directory\\clients.txt");
        }catch(SecurityException se){
            System.err.println("Write permission denied, terminating");
            System.exit(1);
        }catch(FileNotFoundException fnf){
            System.err.println("Error opening file,terminating");
            System.exit(1);
        }
    }
    
    
    // add records to a file
    public static void addRecords(){
        
        char ch = 'y';
        
        Scanner input = new Scanner(System.in);
        System.out.printf("%n%s%n%s%n? ",
                "Enter account number,First Name,Last Name and Balance.",
                "End of File indicator to end input");
        
        
        // loop until end of file indicator
        do{
            
            try{
                
                // output new record to file; assumes valid input
                output.format("%d %s %s %.2f%n", input.nextInt(),input.next()
                ,input.next(),input.nextDouble());
                System.out.println("Any other record");
                ch = input.next().charAt(0);
            }catch(FormatterClosedException fc){
                System.err.println("Error writing to file,terminating");
                break;
            }catch(NoSuchElementException nse){
                System.err.println("Invalid input. Please try again");
                //discards input so that user can try again
                input.nextLine();
            }
            
            System.out.print("? ");
        }while(ch == 'Y'|| ch == 'y' );
    
    }
    
    // close the file
    public static void closeFile(){
        
        if(output!= null){
            output.close();
        }
    }
    
}
