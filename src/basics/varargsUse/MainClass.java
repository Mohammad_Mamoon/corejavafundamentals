/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.varargsUse;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        displayString(5);
        displayString(12,"");
        displayString(3,"hi");
        displayString(17,"Mohammad","Mamoon");
    }

    
    
    // there can be only 1 variable argument in the method 
    // if there more than one argument then it has to be the last one
    private static void displayString(int num,String... string) {
        
        System.out.println(num);
        
        for(String element : string) {
            System.out.println(element);
        }
    }
    
}
