/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.abstractClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        Imp obj = new Imp();
        obj.say();
        obj.greet();
        
        
    }
}

abstract class Try {
    
    void greet() {
        System.out.println("hi there");
    }
    
    abstract void say();

}

class Imp extends Try {

    @Override
    void say() {
        System.out.println("welcome");
    }
    
}


