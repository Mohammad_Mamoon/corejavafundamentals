/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.methodLocalInnerClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class OuterClass {
    
    private static int a = 50;
    private int b = 90;
    int c = 30;
    
    
    void display() {
        
        class InnerClass {
            
            void print() {
                System.out.println(a);
                System.out.println(b);
                System.out.println(c);
            }
        }
        
        InnerClass obj = new InnerClass();
        obj.print();
    }
    
}
