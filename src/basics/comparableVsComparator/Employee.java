/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparableVsComparator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee implements Comparable<Employee> {
    
    private int id;
    private String address;
    private double salary;
    
    Employee(int id ,String add,double sal) {
        this.id = id;
        this.address = add;
        this.salary = sal;
    }
    
    @Override
    public int compareTo(Employee other){
        
        return this.id - other.id;
    }
    
    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public double getSalary() {
        return salary;
    }
    
}
