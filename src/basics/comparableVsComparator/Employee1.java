/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparableVsComparator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee1 {
    private int id;
    private String address;
    private double salary;
    
    Employee1(int id ,String add,double sal) {
        this.id = id;
        this.address = add;
        this.salary = sal;
    }
    
    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public double getSalary() {
        return salary;
    }
    
}
