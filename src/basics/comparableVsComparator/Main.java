/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparableVsComparator;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main {
    public static void main(String[] args) {
        
        ArrayList<Employee> al = new ArrayList<>(); 
        al.add(new Employee(101,"Naseem Bagh",43000));
        al.add(new Employee(401,"Hawal",22000));
        al.add(new Employee(201,"Nishat",88000));
        al.add(new Employee(801,"Mohali",65000));
        
        Collections.sort(al);
        
        System.out.println("Employee's after sorting");
        for(Employee e : al){
            System.out.println("Employee's id =" + e.getId()
            + " Salary =" + e.getSalary() + " Address =" + e.getAddress());
        }
    }
    
}
