/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparableVsComparator;

import java.util.Comparator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class AddressCompare implements Comparator<Employee1> {
    
    @Override
    public int compare(Employee1 e1, Employee1 e2) {

        return e1.getAddress().compareTo(e2.getAddress());
//        if (e1.getSalary() > e2.getSalary()) {
//            return 1;
//        }
//        else if (e1.getSalary() < e2.getSalary()) {
//            return -1;
//        } else {
//            return 0;
//        }

        
    }
    
}
