/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.jaggedArray;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TestJaggedArray {
    
    public static void main(String[] args) {
        
        // declaring a 2D array with odd columns
        int[][] array = new int[4][];
        
        array[0] = new int[1];
        array[1] = new int[2];
        array[2] = new int[1];
        array[3] = new int[2];
        
        // initializing the array
        int count = 0;
        
        for(int i=0; i<array.length; i++) {
            for(int j=0; j<array[i].length; j++) {
                array[i][j] = count++; 
            }
        }
        
        // printing the values
        for(int i=0; i<array.length; i++) {
            for(int j=0; j<array[i].length; j++) {
                System.out.print(array[i][j] + " "); 
            }
            System.out.println();
        }
        
        System.out.println(array.getClass().getName());
    }
    
}
