/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.initializerBlock;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassB extends ClassA {
    
    private int num;
    
    {
        num = 50;
        System.out.println("Initializer block invoked");
    }
    
    ClassB() {
        super();
        // here intializer block is invoked first
        System.out.println("subclass constructor");
    }
    
}
