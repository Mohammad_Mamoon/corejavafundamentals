/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.usingObjectMethods;

import java.time.LocalDate;
import java.time.Month;
import java.util.Objects;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee {
    
    private String name;
    private double salary;
    private LocalDate hireDay;

    public Employee(String name, double salary,int year,int month, int day) {
        this.name = name;
        this.salary = salary;
        this.hireDay = LocalDate.of(year,month,day);
    }
    
    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }
    
    @Override
    public boolean equals(Object otherObject) {
        
        // a quick test to see if the objects are identical
        if(this == otherObject)
            return true;
        
        // must return false if the explicit parameter is null
        if(otherObject == null) 
            return false;
        
        // if the classes don't match, they can't be equal
        if(this.getClass() != otherObject.getClass())
            return false;
        
        // now we know otherObject is a non-null Employee
        Employee other = (Employee) otherObject;
        
        // test whether the fields have identical values
        return Objects.equals(name, other.name) &&
                salary == other.salary &&
                Objects.equals(hireDay, other.hireDay);
    }
    
    public int hashCode() {
        return Objects.hash(name,salary,hireDay);
    }
    
    public String toString() {
        
        return getClass().getName() + "[ Name=" + name + " Salary=" + salary +
                " HireDay=" + hireDay + "]";
    }
    
    
}
