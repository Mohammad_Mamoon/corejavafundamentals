/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author Mohammad_Mamoon
 */
public class EmployeeTest {
    
    public static void main(String args[]) {
    
    Employee[] staff = new Employee[3];
    
    staff[0] =  new Employee("Mamoon",52000,2018,12,01);
    staff[1] =  new Employee("Lubna",53000,2018,12,01);
    staff[2] =  new Employee("Yamin",51000,2018,12,01);
    
    
    
    for(Employee e : staff) {
      e.raiseSalary(5);
    }
    
    
    for(Employee e: staff){
        System.out.println("name=" + e.getName() + " salary=" + e.getSalary() +
                " hireDay=" + e.getHireDay());
        
    }
    
    }
}

class Employee {
    
    private String name;
    private double salary;
    private LocalDate hireDay;
    
    public Employee(String name,int salary,int year,int month,int day) {
        
        this.name = name;
        this.salary = salary;
        this.hireDay = LocalDate.of(year, month, day);
        
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }

    public void raiseSalary(double byPercent){
        
        double raise = (salary*byPercent)/100;
        salary +=raise;
        
    }
    
    
}
