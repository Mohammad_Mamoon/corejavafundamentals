/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class AnonymousTry2 {
    public static void main(String[] args) {
        
        Thread t = new Thread() {
            
        @Override
        public void run() {
            System.out.println("in the run");
        }
    };
    
    t.start();
    
    
        Thread th =new Thread(() -> System.out.println("in the run2")
                
                
                );
    
        th.start();
        
        
        Runnable r = new Runnable() {
            
            public void run(){
                System.out.println("in the run3");
            }
        };
        
        r.run();
        
        
        Runnable ru = () ->  System.out.println("in the run4");
        
        ru.run();
        
        OtherRun obj = new OtherRun();
        
        Runnable rr = () -> {
            
            obj.run1();
            
        };
    
        rr.run();
        
        
        
         // method reference
        Runnable ri = obj::run1;
        
        ri.run();
    
    }
    
}
