/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee2 {
    
    private String name;
    private double salary;

   
    public Employee2(String n,double s){
        name = n;
        salary = s;
        
    }
    
     public double getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }
    
    public void raiseSalary(double byPercent){
        double raise = salary * byPercent / 100;
        salary = salary + raise;
    }
    
    
}
