/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ParamTest {
    
    public static void main(String[] args) {
        
        // methods can alter numeric parameters
        System.out.println("Testing triple value");
        double percent = 10;
        System.out.println("Before : percent=" + percent);
        triplePercent(percent);
        System.out.println("After : percent=" + percent);
        
        
        // methods can change the state of object paramteres
        System.out.println("\nTesting tripleSalary:");
        Employee2 harry =new Employee2("Harry",52000.0);
        System.out.println("Salary before raise: " + harry.getSalary());
        tripleSalary(harry);
        System.out.println("Salary after raise: " + harry.getSalary());
        
        // methods can't attach new objects to object prameters
        System.out.println("Testing swap:");
        Employee2 a = new Employee2("Alice",70000);
        Employee2 b = new Employee2("Bob",54000);
        
        System.out.println("Before : a=" + a.getName());
        System.out.println("Before : b=" + b.getName());
        swap(a,b);
        System.out.println("After : a=" + a.getName());
        System.out.println("After : b=" + b.getName());
        
    }
    
    
        
        
    
    
    
    
    
    
    
    
    public static void triplePercent(double x) {
        
        x= 3 * x;
        System.out.println("x=" + x);
    }
    
    
    public static void tripleSalary(Employee2 x){
        x.raiseSalary(200);
        System.out.println("End of method: salary=" + x.getSalary() );
    }
    
    public static void swap(Employee2 a , Employee2 b) {
        Employee2 temp;
        
        temp = a;
        a= b;
        b= temp;
        System.out.println("End of method : a=" + a.getName());
        System.out.println("End of method : b=" + b.getName());
    }
}
