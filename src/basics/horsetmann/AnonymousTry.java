/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class AnonymousTry {
    public static void main(String[] args) {
        
        InterFace1 obj =  new InterFace1() {
             
            @Override
            public void greet1() {
                System.out.println("What would you like to have!");
            }
            
        };
        
        
        
        InterFace1.myName();
        obj.greet();
        obj.greet1();
        
        
        
        Thread t = new Thread() {
            
            @Override
            public void run(){
                System.out.println("inside the thread");
            }
            
        };
        
        t.start();
        
        Thread p = new Thread(() -> {
        
          
              
              System.out.println("inside the next thread");
          
        
        
        });
        
        p.start();
        
        
        Runnable r = () -> {
            
           
                System.out.println("runnable");
            
        };
        
        r.run();
    }
    
}
