/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StaticTest {
    
    public static void main(String args[]){
        
        Employee1[] staff = new Employee1[2];
        
        staff[0] = new Employee1("Mansha" ,52000);
        staff[1] = new Employee1("Yasir" ,52000);
        
        for(Employee1 e : staff) {
            
            e.setId();
            System.out.println("name=" + e.getName() + " id=" + e.getId() 
            + " salary=" + e.getSalary());
        }
        
        int n = Employee1.getNextId();
        System.out.println("next available id=" + n);
        
        
    }
    
}

class Employee1{
    
    private static int nextId=1;
    
    private String name;
    private double salary;
    private int id;
    
    public Employee1(String n, int s){
        
        name = n;
        salary = s;
        id = 0;
    }

    public static int getNextId() {
        return nextId;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public int getId() {
        return id;
    }
    
    public void setId(){
        id = nextId;
        nextId++;
                
    }
    
    
    
}