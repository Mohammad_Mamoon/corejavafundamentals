/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.GenericsTry;

import java.util.EmptyStackException;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StackTest {
    
    public static void main(String[] args) {
        
        Integer[] intArray = { 3,4,5 };
        Double[] doubleArray = { 4.1, 8.3, 9.8 };
        
        Stack<Integer> integerStack = new Stack<>(3);
        Stack<Double>  doubleStack  = new Stack<>();
        
        testPush("intArray",intArray,integerStack);
        testPop("intArray",integerStack);
        
        testPush("doubleArray",doubleArray,doubleStack);
        testPop("doubleArray",doubleStack);
        
    }
    
    
    public static <T> void testPush(String name,T[] values,Stack<T> stack) {
        
        System.out.printf("%nPushing elements onto %s%n",name);
        
        for(T value : values) {
            System.out.printf("%s ",value);
            stack.pushValue(value);
        }
    }
    
    public static <T> void testPop(String name,Stack<T> stack) {
       
        try{
            
            System.out.printf("%nPoping elements from %s%n",name);
            T popValue;
            
            while(true) {
                popValue = stack.popValue();
                System.out.println(popValue);
            }
        }catch(EmptyStackException e) {
            System.err.println();
            e.printStackTrace();
        }
        
    }
    
}
