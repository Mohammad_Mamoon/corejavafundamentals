/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.GenericsTry;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Stack<T> {
    
    private final ArrayList<T> elements;
    
    Stack() {
        this(3);
    }
    
    Stack(int capacity) {
        int newCapacity = capacity > 0 ? capacity : 10;
        elements = new ArrayList(newCapacity);
    }
    
    public void pushValue(T value) {
        elements.add(value);
    }
    
    public T popValue() {
        if(elements.isEmpty())
            throw new generics.genericStack.EmptyStackException("Stack is empty, cannot pop");
        
        return elements.remove(elements.size()-1);
    }
    
}
