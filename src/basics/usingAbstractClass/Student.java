/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.usingAbstractClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Student extends Person{
    private String major;

    public Student(String name , String major) {
        super(name);
        this.major = major;
    }
    
    public String getDescription() {
        return " a student majoring in " + major;
    }
    
}
