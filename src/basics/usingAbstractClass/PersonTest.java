/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.usingAbstractClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class PersonTest {
    public static void main(String[] args) {
    
    Person[] people = new Person[2];
    people[0] = new Employee("Mamoon",78111,2018,11,13);
    people[1] = new Student("Babar","Computer Science");
    
    // print out the names and description of all Person objects
    for(Person p : people)
     System.out.println(p.getName() + " ," + p.getDescription());
     
    }
}
