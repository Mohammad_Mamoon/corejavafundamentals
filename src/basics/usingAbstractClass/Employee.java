/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.usingAbstractClass;

import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee extends Person {
    private double salary;
    private LocalDate hireDay;

    public Employee(String name,double salary,int year,int month,int day) {
        super(name);
        this.salary = salary;
        this.hireDay = LocalDate.of(year, month, day);
    }
    
    public String getDescription() {
        return String.format("an employee with a salary of $%.2f" ,salary);
    }
    
}
