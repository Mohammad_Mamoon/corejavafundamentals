/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.superKeyword;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassB extends ClassA {
    
    
    public void display() {
        
        System.out.println(super.color);
        super.greet();
    }

    public ClassB() {
       
        //super(); 
        // here if we dont mention super or this keyword, compiler
        //automatically adds the super keyword
        System.out.println("subclass constructor");
    }
    
    
}
