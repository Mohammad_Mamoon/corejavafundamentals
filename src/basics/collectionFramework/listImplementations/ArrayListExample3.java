/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.listImplementations;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayListExample3 {
    
    
    public static void main(String[] args) {
        
        
        // Different ways to add elements
        
        ArrayList<String> list = new ArrayList();
        System.out.println("Initial list of elements: " + list);
        
        //adding elements to the end of the list
        list.add("Hi");
        list.add("There");
        list.add("i see you");
        System.out.println("After invoking add(E element) method: " + list);
        
        //adding element at the specfied position
        list.add(2, "home is home");
        System.out.println("After invoking add(int index,E element) method: " + list);
        
        ArrayList<String> list2 = new ArrayList();
        list2.add("Chinar");
        list2.add("trees");
        
        //adding second list to the end of the first list
        list.addAll(list2);
        System.out.println("After invoking addAll(Collection <? extends E> c) method"
                + ": " + list);
        
        ArrayList<String> list3 = new ArrayList();
        list3.add("Naseem Bagh");
        list3.add("Nishat");
        
        //adding third list elements to the first list at the specified position
        list.addAll(1, list3);
        System.out.println("After invoking addAll(int index, "
                + "Collection(<? extends E> c) method: " + list);
        
        
        
        System.out.println("------");
        System.out.println("Before update: " + list.get(3));
        
        // updating an element at specific location
        list.set(3, "KU");
        System.out.println("After update: " + list.get(3));
        
        
        
        
        System.out.println("---------");
        //Different ways to remove elements
        
        //removing specific element
        list.remove("i see you");
        System.out.println("After invoking remove(Object) method: " + list);
        
        //removing on the basis of specific position
        list.remove(4);
        System.out.println("After invoking remove(int index) method: " + list);
        
        //removing all the new elements from ArrayList
        list.removeAll(list3);
        System.out.println("After invoking removeAll() method: " + list);
        
        //removing all the elements in the list
        list.clear();
        System.out.println("After invoking clear() method: " + list);
    }
    
}
