/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.listImplementations;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayListExample2 {
    
    public static void main(String[] args) {
        
        Employee emp1 = new Employee(1,"Tahir");
        Employee emp2 = new Employee(2,"Yasir");
        Employee emp3 = new Employee(3,"Amir");
        Employee emp4 = new Employee(4,"Taubah");
        
        ArrayList<Employee> list = new ArrayList();
        list.add(emp1);
        list.add(emp2);
        list.add(emp3);
        list.add(emp4);
        
        Iterator itr = list.iterator();
        while(itr.hasNext()) {
            Employee emp = (Employee)itr.next();
            System.out.println(emp.getEmpId() + " " + emp.getName());
        }
        
        
        
        
    }
    
}

class Employee {
    
    private int empId;
    private String name;

    public Employee(int empId, String name) {
        this.empId = empId;
        this.name = name;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
