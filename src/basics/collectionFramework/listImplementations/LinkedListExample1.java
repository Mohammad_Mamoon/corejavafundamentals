/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.listImplementations;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class LinkedListExample1 {
    
    public static void main(String[] args) {
        
         //creating LinkedList
        LinkedList<String> list = new LinkedList();
        
        //adding objects in LinkedList
        list.add("Srinagar");
        list.add("paris");
        list.add("Srinagar");
        list.add("florida");
        
        //Traversing list through Iterator
        Iterator iterator = list.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        System.out.println("--------");
        
        //traversing elements in both forward direction and backward direction
        ListIterator iter = list.listIterator();
        while(iter.hasNext()) {
            System.out.println(iter.next());
        }
        while(iter.hasPrevious()) {
            System.out.println(iter.previous());
        }
        
        System.out.println("--------");
        
        //Traversing the list in the reverse order
        Iterator itr = list.descendingIterator();
        while(itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        System.out.println("----------");
        
        // Different ways to add elements
        
       
        
        //adding elements to the end of the list
        list.add("Hi");
        list.add("There");
        list.add("i see you");
        System.out.println("After invoking add(E element) method: " + list);
        
        //adding element at the specfied position
        list.add(2, "home is home");
        System.out.println("After invoking add(int index,E element) method: " + list);
        
        LinkedList<String> list2 = new LinkedList();
        list2.add("Chinar");
        list2.add("trees");
        
        //adding second list to the end of the first list
        list.addAll(list2);
        System.out.println("After invoking addAll(Collection <? extends E> c) method"
                + ": " + list);
        
        LinkedList<String> list3 = new LinkedList();
        list3.add("Naseem Bagh");
        list3.add("Nishat");
        
        //adding third list elements to the first list at the specified position
        list.addAll(1, list3);
        System.out.println("After invoking addAll(int index, "
                + "Collection(<? extends E> c) method: " + list);
        
        
        
        System.out.println("------");
        System.out.println("Before update: " + list.get(3));
        
        // updating an element at specific location
        list.set(3, "KU");
        System.out.println("After update: " + list.get(3));
        
        System.out.println("------");
        //adds at initial position
        list.addFirst("Play");
        
        //adds at last position
        list.addLast("Hallo");
        System.out.println(list);
        
        System.out.println("---------");
        //Different ways to remove elements
        
        //removing specific element
        list.remove("i see you");
        System.out.println("After invoking remove(Object) method: " + list);
        
        //removing on the basis of specific position
        list.remove(4);
        System.out.println("After invoking remove(int index) method: " + list);
        
        //removing all the new elements from ArrayList
        list.removeAll(list3);
        System.out.println("After invoking removeAll() method: " + list);
        
        //remove first element in the list
        list.removeFirst();
        System.out.println(list);
        
        //remove last element
        list.removeLast();
        System.out.println(list);
        
        //remove first occurance of the element from the list
        list.removeFirstOccurrence("Hi");
        System.out.println(list);
        
        list.removeLastOccurrence("Chinar");
        System.out.println(list);
        
        //removing all the elements in the list
        list.clear();
        System.out.println("After invoking clear() method: " + list);
        
    }
    
}
