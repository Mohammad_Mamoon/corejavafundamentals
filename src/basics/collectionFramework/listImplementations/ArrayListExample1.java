/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.listImplementations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayListExample1 {
    
    public static void main(String[] args) {
        
        //creating ArrayList
        ArrayList<String> list = new ArrayList();
        
        //adding objects in ArrayList
        list.add("Srinagar");
        list.add("paris");
        list.add("Srinagar");
        list.add("florida");
        list.add(null);
        list.add(null);
        
        //Traversing list through Iterator
        Iterator iterator = list.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        
        System.out.println("-----------");
        
        //Traversing list with for each loop
        for(String element : list)
            System.out.println(element);
        
        System.out.println("-----------");
        
        //Traversing list through ListIterator Interface
        ListIterator itr = list.listIterator();
        while(itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        System.out.println("-----------");
        
        //Traversing with for loop
        for(int i=0; i<list.size(); i++ ) {
            System.out.println(list.get(i));
        }
        
        System.out.println("-----------");
    }
    
}
