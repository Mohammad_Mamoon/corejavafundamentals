/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.mapImplementations;

import java.util.TreeMap;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TreeMapExample1 {
    
    public static void main(String[] args) {
        
        TreeMap<Integer,String> map = new TreeMap();
        
    
        map.put(5,"canberra");
        map.put(6, "zambia");
        map.put(22, "Los Angeles");
        
        // performing NavigableMap operations
        
        System.out.println("descendingMap: " + map.descendingMap());
        
        System.out.println("HeadMap: " + map.headMap(102,true));
        
        System.out.println("SubMap: " + map.subMap(100, false, 102, true));
        
        System.out.println("TailMap: " + map.tailMap(102, true));
        
        
        System.out.println("--------");
        
        // SortedSet operations
        
        System.out.println("HeadMap: " + map.headMap(102));
         
        System.out.println("SubMap: " + map.subMap(100,102));
        
        System.out.println("TailMap: " + map.tailMap(102));
        
        
    }
    
}
