/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.mapImplementations;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TreeMapExample2 {
    
    public static void main(String[] args) {
        
        
        // Key elements in the TreeMap must be of Comparable type
        // String and Wrapper classes are Comparable by default
        // so adding user-defined key objects in the TreeSet, we need to
        // implement the Comparable interface
        
        TreeMap<Id,Book> map = new TreeMap();
        map.put(new Id(2),new Book(121,"Let us C","Yk","BPB",8));
        map.put(new Id(16),new Book(233,"Operating System","Galvin","Wiley",6));
        map.put(new Id(1),new Book(101,"C++","abc","Arihant",12));
        
        for(Map.Entry entry : map.entrySet()) {
            Id i = (Id) entry.getKey();
            Book b = (Book) entry.getValue();
            
            System.out.println(i.getId() + " Details:");
            System.out.println(b.getId() + " " + b.getName()
            + " " + b.getAuthor() + " " + b.getPublisher() +
                    " " + b.getQuantity());
        }
        
        
        
    }
    
}

class Book  {
    
    private int id;
    private String name, author, publisher;
    private int quantity;

    public Book(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    
}


class Id implements Comparable<Id> {
    
    private int id;

    public Id(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  

    @Override
    public int compareTo(Id obj2) {
        
        if(this.getId() > obj2.getId())
            return 1;
        
        else if(this.getId() < obj2.getId())
            return -1;
        
        else
            return 0;
    }
    
    
}

    

