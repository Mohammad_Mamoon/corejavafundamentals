/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.mapImplementations;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mohammad_Mamoon
 */
public class HashMapExample2 {
    
    public static void main(String[] args) {
        
        HashMap<Integer,String> map = new HashMap();
        
        map.put(1, "London");
        map.put(5,"canberra");
        map.put(6, "zambia");
        map.put(22, "Los Angeles");
        
        for(Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        
        
    }
    
}
