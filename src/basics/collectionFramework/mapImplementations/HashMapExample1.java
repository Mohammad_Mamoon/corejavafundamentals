/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.mapImplementations;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Mohammad_Mamoon
 */
public class HashMapExample1 {
    
    public static void main(String[] args) {
        
        Map map = new HashMap();
        
        map.put(1, "London");
        map.put("k","canberra");
        map.put(1.2, "zambia");
        
        // converts map to a set to be traversed
        Set set = map.entrySet();
        
        Iterator itr = set.iterator();
        while(itr.hasNext()) {
            
            // converting to Map.entry so that we can get key and value 
            // separately
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
    
}
