/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.collectionFramework.mapImplementations;


import java.util.LinkedHashMap;
import java.util.Map;


/**
 *
 * @author Mohammad_Mamoon
 */
public class LinkedHashMapExample1 {
    
    public static void main(String[] args) {
        
        LinkedHashMap<Integer,String> map = new LinkedHashMap();
        
    
        map.put(5,"canberra");
        map.put(6, "zambia");
        map.put(22, "Los Angeles");
        
        // allows single null key but multiple null values
        map.put(7, null);
        map.put(null, "carlos");
        
        for(Map.Entry entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        
        //fetching keys only
        System.out.println(map.keySet());
        
        //fetching values only
        System.out.println(map.values());
        
        //fetching key - value pairs
        System.out.println(map.entrySet());
    
        
    }
    
    
}
