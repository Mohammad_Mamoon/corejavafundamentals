/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.enumUse;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Example2 {
    
    
    // initializing specific values to enum constants
    // and we can do so by defining fields and constructors
    public enum Season {
        Winter(12), Spring(4), Summer(8), Fall(31);
        
        private int value;
        
        private Season(int value) {  // constructor of enum has to be private
            this.value = value;
        }
    }
    
    
    public static void main(String[] args) {
        
        for(Season s: Season.values()) {
            System.out.println(s);
        }
    }
    
}
