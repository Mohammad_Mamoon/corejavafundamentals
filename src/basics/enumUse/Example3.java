/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.enumUse;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Example3 {
    
    public enum Day { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }
    
    public static void main(String[] args) {
        
        Day day = Day.Monday;
        
        switch(day) {
            
            case Friday:
                System.out.println("Its Friday");
                break;
                
            case Thursday:
                System.out.println("Its Thursday");
                break;
                
            case Monday:
                System.out.println("Its Monday");
                break;
                
            default:
                System.out.println("other day");
        }
    }
    
}
