/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.enumUse;

/**
 *
 * @author Mohammad_Mamoon
 */

enum Season { Winter, Spring, Summer, Fall }   // defined outside class

public class Example1 {
    
    // defined within the class
    //public enum Season { Winter, Spring, Summer, Fall }
    
    public static void main(String[] args) {
        
        // when an enum is created, values() method is added to it
        //  it returns an array all the values
        for(Season s : Season.values()) {
            System.out.println(s);
        }
    }
    
}
