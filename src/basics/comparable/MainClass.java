/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        List<Student> list = new ArrayList();
        Student std1 = new Student("Mamoon",190006);
        Student std2 = new Student("Nasir",190001);
        Student std3 = new Student("Bilal",190004);
        
        list.add(std1);
        list.add(std2);
        list.add(std3);
        
        Collections.sort(list);
        
        for(Student std : list) {
            System.out.println("name=" + std.getName() + " " + "pincode=" + 
                    std.getPincode());
        }
    }
}
