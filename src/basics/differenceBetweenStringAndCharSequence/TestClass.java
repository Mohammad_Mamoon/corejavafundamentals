/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.differenceBetweenStringAndCharSequence;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TestClass {
    public static void main(String[] args) {
        
        // the first line creates a string literal which is String object
        // and since it implements a charSequence , its a CharSequence too
        CharSequence obj = "hello";
        
        // since , strings are pooled , the second line points to the same 
        // string literal(object) created before
        String str = "hello";
        
        // so, therefore both obj and str are references to the same object
        // and they are true on equals and ==
        System.out.println("Type of obj: " + obj.getClass().getSimpleName());
        System.out.println("Type of str: " + str.getClass().getSimpleName());
        System.out.println("Value of obj: " + obj);
        System.out.println("Value of str: " + str);
        System.out.println("Is obj a String? " + (obj instanceof String));
        System.out.println("Is obj a CharSequence? " + (obj instanceof CharSequence));
        System.out.println("Is str a String? " + (str instanceof String));
        System.out.println("Is str a CharSequence? " + (str instanceof CharSequence));
        System.out.println("Is \"hello\" a String? " + ("hello" instanceof String));
        System.out.println("Is \"hello\" a CharSequence? " + ("hello" instanceof CharSequence));
        System.out.println("str.equals(obj)? " + str.equals(obj));
        System.out.println("(str == obj)? " + (str == obj));
        
        
        
    }
    
}
