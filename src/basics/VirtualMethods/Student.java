/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.VirtualMethods;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Student extends Person {
    
    private String grade;
    
    Student(String name, String address, String grade) {
        super(name,address);
        this.grade = grade;
    }
    
    public void details() {
        System.out.println("name: " + getName() + " ,address: " + getAddress() + " ,grade: " + grade);
    }
}
