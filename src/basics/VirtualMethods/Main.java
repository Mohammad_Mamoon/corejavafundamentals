/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.VirtualMethods;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main {
    public static void main(String[] args) {
        
        // here on both the instances of object creation and calling at compile
        // time validation of the method calls is done with the refrence types
        // but at the runtime overriden methods of the created objects are called
        // this behavior is known as virtual method invocation
        // and these methods are called virtual methods
        
        Student object1 = new Student("Mamoon","Naseem Bagh","Post Graduate");
        object1.details();
        
        Person object2 = new Student("Danish","Nishat Bagh","Post Graduate");
        object2.details();
    }
    
}
