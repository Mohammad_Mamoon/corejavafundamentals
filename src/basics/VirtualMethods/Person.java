/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.VirtualMethods;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Person {
    
    private String name;
    private String address;
    
    Person(String name , String address) {
        this.address = address;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
    
    public void details() {
        System.out.println("Name: " + name + " ,address: " + address);
    }
    
    
}
