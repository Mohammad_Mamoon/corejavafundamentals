/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.stringBuilderUse;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StringDemo {
    public static void main(String[] args){
        String palindrome = "Dot saw I was Tod";
        int length = palindrome.length();
      
        char[] tempCharArray = new char[length];
        char[] charArray = new char[length];
        
        // put original string in an array of chars
        for(int i = 0; i < length; i++){
            tempCharArray[i] = palindrome.charAt(i);
        }
        
        //reverse array of charcters
         for(int i = 0; i < length; i++){
            charArray[i] = tempCharArray[length - 1 - i];
        }
         
         String reversePalindrome = new String(charArray);
         System.out.println(reversePalindrome);
        
        
                
    }
    
}
