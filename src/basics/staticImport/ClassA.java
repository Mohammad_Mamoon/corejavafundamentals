/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.staticImport;

import static java.lang.Math.*;
import static java.lang.System.out;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassA {
    
    public static void main(String[] args) {
        
        // using static functions of Math class without using the classname
        // itself or its object
        
        System.out.println(sqrt(16));
        System.out.println(pow(3,2));
        System.out.println(abs(5.2));
        
        // here we are using the static variable out of System class without
        // mentioning the classname itself
        
        out.println("hi there");
    }
    
}
