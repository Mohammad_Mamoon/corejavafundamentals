/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.basicPolymorphism;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassC extends ClassB implements InterfaceA{
    public static void main(String[] args) {
          
        // Object ClassC is polymorphic in nature as it has the ability
        // to exist in many forms
        // reference variable to the object can be of Superclass type or of
        // interface type
        
        ClassX obj = new ClassC();
        //InterfaceA obj = new ClassC();
        //obj.message();
        //obj.season();
        //obj.prompt();
        obj.func();
    }
    
    public void message() {
        System.out.println("2nd November");
    }
    
    public void season() {
        System.out.println("Winter");
    }
    
}
