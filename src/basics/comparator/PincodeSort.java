/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparator;

import java.util.Comparator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class PincodeSort implements Comparator<Student> {

    @Override
    public int compare(Student object1, Student object2) {
        
        if (object1.getPincode() > object2.getPincode())
           return 1;
       else if(object1.getPincode() < object2.getPincode())
           return -1;
       else
           return 0;
    }
    
}
