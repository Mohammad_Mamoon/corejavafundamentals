/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparator;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Student implements Comparable<Student> {
    
    private String name;
    private int pincode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public Student(String name, int pincode) {
        this.name = name;
        this.pincode = pincode;
    }
    
    
    
    

    @Override
    public int compareTo(Student object) {
        
        return this.getName().compareTo(object.getName());
       
//       if (this.getPincode() > object.getPincode())
//           return 1;
//       else if(this.getPincode() < object.getPincode())
//           return -1;
//       else
//           return 0;
    }
    
}
