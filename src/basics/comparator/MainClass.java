/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import basics.comparator.PincodeSort;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        List<Student> list = new ArrayList();
      
        
        list.add(new Student("Bilal",190009));
        list.add(new Student("Mamoon",190001));
        list.add(new Student("Nasir",190003));
        list.add(new Student("Haziq",590001));
        
        
        System.out.println("Sorting done on the names");
        Collections.sort(list);
        
        for(Student std : list) {
            System.out.println("name=" + std.getName() + " " + "pincode=" + 
                    std.getPincode());
        }
        
        System.out.println();
        System.out.println();
        
        System.out.println("Sorting done on pincodes");
        Collections.sort(list,new PincodeSort());
        
        for(Student std : list) {
            System.out.println("name=" + std.getName() + " " + "pincode=" + 
                    std.getPincode());
        }
    }
}
