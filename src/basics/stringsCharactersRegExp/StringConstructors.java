/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.stringsCharactersRegExp;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StringConstructors {
    
    public static void main(String[] args) {
        
        char[] charArray = {'b','i','r','t','h',' ','d','a','y'};
        String s = new String("hello");
        
        // use String constructors
        
        // here a String object is created using String classe's no-argument
        // constructor which is an empty string of length 0 and its reference
        // is assigned s1
        String s1 = new String();
        
        // here another String object is created using the String class's
        // constructor whicht takes the String object as its argument
        String s2 = new String(s);
        
        // here String object is created using the String class's constructor 
        // that takes character array as its argument
        // the new String contains the copy of characters in the array
        String s3 = new String(charArray);
        
        // here a String object is created using the class String's constructor
        // takes takes three arguments
        // first is the character array , second is the position of the array
        // where from the elements are accessed , third is the no of elements
        // to be accessed
        // If the offset or the count specified results in the access of character
        // array
        // elements beyond its bounds a StringIndexOutOfBoundsException occurs
        String s4 = new String(charArray,6,3);
        
        System.out.printf(
                "s1 = %s%ns2 = %s%ns3 = %s%ns4 = %s%n", s1, s2, s3, s4);
    }
    
}
