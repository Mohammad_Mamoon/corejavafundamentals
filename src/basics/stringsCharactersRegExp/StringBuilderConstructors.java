/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.stringsCharactersRegExp;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StringBuilderConstructors {
    public static void main(String[] args) {
        
        // StringBuilder provides four types of constructors
        // this constructor takes no argument with an initial capacity of 16 
        // characters, it doesn't have any characters
        StringBuilder buffer1 = new StringBuilder();
        
        // this constructor takes one integer argument
        // that specifies the initial capacity of the StringBuilder
        StringBuilder buffer2 = new StringBuilder(10);
        
        // it takes a String argument with the intial capacity equal
        // to number of characters in the string plus 16
        StringBuilder buffer3 = new StringBuilder("hello");
        
        System.out.printf("buffer1 = \"%s\"%n", buffer1);
        System.out.printf("buffer2 = \"%s\"%n", buffer2);
        System.out.printf("buffer3 = \"%s\"%n", buffer3);
    }
    
}
