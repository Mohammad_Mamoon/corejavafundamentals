/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.aggregation;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Student {
    
    private String name;
    private Address address;

    public Student(String name, Address address) {
        this.name = name;
        this.address = address;
    }
    
    public void display() {
        System.out.println("name:" + name + " " + " city:" + "" + address.city
         + "  pincode:" + "" + address.pincode);
    }
    
    
    
}
