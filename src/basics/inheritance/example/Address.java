/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.inheritance.example;

/**
 *
 * @author Mohammad_Mamoon
 */
class Address {
    
    private String state;
    private String country;

    public Address(String state, String country) {
        this.state = state;
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }
    
    
    
}
