/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.inheritance.example;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main {
    public static void main(String[] args) {
        
        Address address = new Address("JK","India");
        
        ClassB obj = new ClassB(102,"Pinki",109.1,address);
        System.out.println("id= " + obj.getId() + " name= " + obj.getName()
        + " salary= " + obj.getSalary() + " state= " + address.getState() 
        + " country= " + address.getCountry());
        
        
    }
    
}
