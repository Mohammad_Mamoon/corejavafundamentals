/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.inheritance.example;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassB extends ClassA {
    
    private double salary;
    private Address address;

    public ClassB(int id, String name,double salary,Address address) {
        super(id, name);
        this.salary = salary;
        this.address = address;
    }

    public double getSalary() {
        return salary;
    }    
    
    
}
