/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.inheritance.example;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassA {
    
    private int id;
    private String name;

    public ClassA(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    

   
    
    
    
}
