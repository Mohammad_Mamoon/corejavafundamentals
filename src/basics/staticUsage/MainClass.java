/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.staticUsage;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    private static int i;
    static {
        i = 50;
        System.out.println("executed before main");
    }
    
    public static void main(String[] args) {
        
        TryClass object1 = new TryClass();
        TryClass object2 = new TryClass();
        System.out.println(object1.sum());
        System.out.println(object2.sum());
        TryClass.greet();
        
    }
    
}
