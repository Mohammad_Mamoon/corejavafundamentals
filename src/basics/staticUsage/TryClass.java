/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.staticUsage;

/**
 *
 * @author Mohammad_Mamoon
 */
public class TryClass {
    
    private int num1 = 10;
    private static int num2= 30;
    
    public int sum() {
        num2= num2 + 30;
        return num1 + num2;
    }
    
    public static void greet() {
        System.out.println("hi there " + num2);
    }
    
}
