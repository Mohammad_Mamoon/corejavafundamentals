/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.assertions;

import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Enter your weight");
        int weight = scan.nextInt();
        
        
        // assertion is a statement used to test the correctness
        // of any assumptions made in the program
        assert weight >= 30 : "your underweight bro";
        
        System.out.println("Your weight is " + weight);
        
        
    }
    
}
