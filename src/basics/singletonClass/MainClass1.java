/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.singletonClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass1 {
    public static void main(String[] args) {
        
        ClassA obj = ClassA.getInstance();
        obj.setStr();
        System.out.println(obj.getStr());
        
        ClassA obj2 = ClassA.getInstance();
        System.out.println(obj2.getStr());
        
        
        
    }
    
}
