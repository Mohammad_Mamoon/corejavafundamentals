/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.singletonClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Singleton {
    
    
    // variable instance which is set to null initially and would point to 
    // the instance of the Singleton class
    public static Singleton instance = null;
    
    public String string;
    
    
    // constructor made private 
    private Singleton() {
        string = "A String";
    }
    
    
    // static method getInstance() defined which returns the instance of
    // Singleton Class
    public static Singleton getInstance() {
        
        if(instance == null) 
            instance = new Singleton();
        
        return instance;
    } 
    
}
