/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.singletonClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        Singleton object1 = Singleton.getInstance();
        Singleton object2 = Singleton.getInstance();
        
        System.out.println("Object1 has a string = " + object1.string);
        System.out.println("Object2 has a string = " + object2.string);
        
    }
    
}
