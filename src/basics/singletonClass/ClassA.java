/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.singletonClass;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ClassA {
    
    private static ClassA instance;
    private String str;
    
    private ClassA() {
        
    }
    
    public static ClassA getInstance() {
        if(instance==null)
            instance = new ClassA();
        
        return instance;
    }
    
    public void setStr() {
        str = "hello world";
    }
    
    public String getStr() {
        return str;
    }
    
    
}
