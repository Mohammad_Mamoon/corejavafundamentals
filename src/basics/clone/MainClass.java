/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.clone;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        try {
            Student obj = new Student("Mamoon", 190006);
            Student obj1 = (Student) obj.clone();
            
            System.out.println(obj);
            System.out.println(obj1);
            
        }catch(CloneNotSupportedException c) {
            
        }
       
    }
    
}
