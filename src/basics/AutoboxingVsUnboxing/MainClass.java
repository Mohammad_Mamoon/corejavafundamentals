/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.AutoboxingVsUnboxing;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class MainClass {
    
    public static void main(String[] args) {
        
        int num = 3;
        
        ClassA obj = new ClassA();
        
        // here the number which is a primitive type is changed to its
        // corresponding wrapper type
        obj.display(num);
        
        int num1 = 89;
        Integer i = new Integer(num1);
        System.out.println(i);
        
        
        int k = i; //unboxing
        System.out.println(k);
        
        
        ArrayList<Character> list = new ArrayList<>(); 
        char[] array = { 'a','p','t' };
        
        for(char element : array) {
            //list.add(element);
            // or
            list.add(Character.valueOf(element));
        }
        
        System.out.println(list);
    }
    
    
    
}
