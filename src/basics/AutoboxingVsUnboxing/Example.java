/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.AutoboxingVsUnboxing;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Example {
    
    public static void main(String[] args) {
        
        ArrayList<Integer> list = new ArrayList<>();
        
        for(int i=0; i<10; i++) {
            list.add(i);
            
            //or
            //list.add(Integer.valueOf(i));
        }
        
        int sumOfOddValues = getResult(list);
        System.out.printf("Sum of odd values is %d \n",sumOfOddValues);
        
    }

    private static int getResult(ArrayList<Integer> list) {
        
        int sum = 0;
        
        for(Integer i : list) {
            
            // auto unboxing of i done here
            if(i % 2 != 0) {
                sum += i;
            }
            
            // or
            // doing it implicitly
            
            // if(i.intValue() % 2 != 0 {
            // sum += i.intValue();
        }
        
        return sum;
    }
}
