/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.dataStructures;

import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class SinglyLinkedListExample1 {
    
    public static void main(String[] args) {
        
        int choice, number;
        char ch;
        
        
        do {
            System.out.println("1-> Insert an element");
            System.out.println("2-> Delete an element by key value");
            System.out.println("3-> Delete an element by key position");
            System.out.println("4-> Print the list");
            System.out.println("5-> Exit");
            
            Scanner scan = new Scanner(System.in);
            choice = scan.nextInt();
            
            LinkedList list = new LinkedList();
            int i =10;
            
            switch(choice) {
                case 1:
                    System.out.println("Give the element");
                    number = scan.nextInt();
                    list.insert(list,number); 
                    System.out.println(i);
                    break;
                    
                case 2:
                    System.out.println("Give the key value");
                    number = scan.nextInt();
                    list.deleteByKey(list,number); 
                    System.out.println(i);
                    break;
                    
                case 3:
                    System.out.println("Give the key position");
                    number = scan.nextInt();
                    list.deleteByPosition(list,number); 
                    System.out.println(i);
                    
                    break;    
                    
                case 4:
                    list.printList(list);
                    System.out.println(i);
                    break;
                    
                case 5:
                    System.exit(0);
                    
                default:
                    System.out.println("Enter a valid choice");
                    
            }
            
            System.out.println("Do you wish to continue, press Y or N");
            
            ch = scan.next().charAt(0);
        }while(ch == 'Y' || ch == 'y' );
        

        
    }
    
}

class LinkedList {
    
    Node head;
    
    static class Node {
        
        int data;
        Node next;
        
        public Node(int data) {
            this.data = data;
            next = null;
        }
        
    }
    
    
    public LinkedList insert(LinkedList list, int data) {
        
        
        Node newNode = new Node(data);
        newNode.next = null;
        
        if(list.head == null) {
            list.head = newNode;
        }
        
        else {
            Node last = list.head;
            while(last.next != null) {
                last = last.next;
            }
            
            last.next = newNode;
        }
        return list;
    }
    
    public void printList(LinkedList list) {
        
        Node currNode = list.head;
        
        if(currNode == null)
            System.out.println("List is empty");
        
        else {
            System.out.println("LinkedList: ");
            
            while(currNode != null) {
                System.out.println(currNode.data  + " ");
                currNode = currNode.next;
            }
            
        }
    }
    
    public LinkedList deleteByKey(LinkedList list, int key) {
        
        Node currNode = list.head;
        Node prev = null;
        
        if(currNode != null && currNode.data == key) {
            list.head = currNode.next;
            System.out.println(key + " found and deleted");
            return list;
        }
        
        while(currNode != null && currNode.data != key) {
            prev = currNode;
            currNode = currNode.next;
        }
        
        if(currNode != null) {
            prev.next = currNode.next;
            System.out.println(key +" found and deleted"); 
        }
        
        if(currNode == null) {
            System.out.println(key +" not found");
        }
        return list;
    }
    
    
    public LinkedList deleteByPosition(LinkedList list, int index) {
        
        Node currNode = list.head;
        Node prev = null;
        
        if(index == 0 && currNode != null) {
            list.head = currNode.next;
            System.out.println(index +" position element deleted");
            
            return list;
        }
        
        int counter = 0;
        
        while(currNode != null ) {
            
            if(counter == index) {
                prev.next = currNode.next;
                System.out.println(index + " position element deleted");
                
                break;
            }
            
            else {
                prev = currNode;
                currNode = currNode.next;
                counter++;
                        
            }
        }
        
        if(currNode == null) {
            System.out.println(index + " position element not found");
        }
        
        return list;
    }
    
}
