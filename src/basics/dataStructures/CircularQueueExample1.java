/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.dataStructures;

import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class CircularQueueExample1 {
    
    public static void main(String[] args) {
        
        NewQueue newQueue = new NewQueue(4);
        int element, choice;
        char ch;
        
        do {
            System.out.println("1-> Enqueue an element");
            System.out.println("2-> Dequeue an element");
            System.out.println("3-> Exit");
            
            Scanner scan = new Scanner(System.in);
            choice = scan.nextInt();
            
            switch(choice) {
                case 1:
                    if( (newQueue.getRearPosition() + 1) % newQueue.getQueueSize()
                            == newQueue.getFrontPosition()) {
                        System.out.println("Can't enqueue");
                    }
                    
                    else {
                        System.out.println("Enter the element to be enqueued");
                        element = scan.nextInt();
                        newQueue.insert(element);
                    }
                    
                    break;
                    
                case 2:
                    if(newQueue.getFrontPosition() == newQueue.getRearPosition()) {
                        System.out.println("Can't dequeue, queue empty");
                    }
                    
                    else {
                        System.out.printf("Removed element is: ");
                        element = newQueue.remove();
                        System.out.print(element+ "\n");
                        
                    }
                    
                    break;
                    
                case 3:
                    System.exit(0);
                    
                default:
                    System.out.println("Enter a valid choice");
                    
            }
            
            System.out.println("Do you wish to continue, press Y or N");
            ch = scan.next().charAt(0);
            
        }while(ch == 'Y' || ch == 'y' );
        
        
    }
    
    
}

class NewQueue {
    
    private int queue[];
    private int front,rear,value;

    public NewQueue(int size) {
        queue = new int[size];
        front = rear = -1;
    }

    public int getRearPosition() {
        return rear;    
    }
    public int getFrontPosition() {
        return front;    
    }
    
    public int getQueueSize() {
        return queue.length;
    }
    
    public void insert(int element) {
        rear = (rear + 1) % queue.length;
        queue[rear] = element;
        
    }
    
    public int remove() {
        front = (front + 1) % queue.length;
        value = queue[front];
        return value;
    }
    
}
