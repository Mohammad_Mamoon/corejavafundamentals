/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.dataStructures;

import java.util.Scanner;

/**
 *
 * @author Mohammad_Mamoon
 */
public class StackExample1 {
    
    public static void main(String[] args) {
        
        Stack stack = new Stack(3);
        char ch;
        int choice, element;
        
        do {
            System.out.println("1-> Insert an element");
            System.out.println("2-> Remove an element");
            System.out.println("3-> Exit");
            
            Scanner scan = new Scanner(System.in);
            choice = scan.nextInt();
            
            switch(choice) {
                case 1:
                    if(stack.getTop() == stack.getStackSize()) {
                        System.out.println("Can't insert, stack overflow");
                    }
                    
                    else {
                        System.out.println("Enter the element to be inserted");
                        element = scan.nextInt();
                        stack.push(element);
                    }
                    
                    break;
                    
                case 2:
                    if(stack.getTop() == -1) {
                        System.out.println("Can't pop, stack underflow");
                    }
                    
                    else {
                        System.out.printf("Poped element is: ");
                        element = stack.pop();
                        System.out.print(element+ "\n");
                        
                    }
                    
                    break;
                    
                case 3:
                    System.exit(0);
                    
                default:
                    System.out.println("Enter a valid choice");
                    
            }
            
            System.out.println("Do you wish to continue, press Y or N");
            ch = scan.next().charAt(0);
            
        }while(ch == 'Y' || ch == 'y' );
    }
}

class Stack {
    
    private int[] stack;
    private int top;
    private int element;

    public Stack(int size) {
        stack = new int[size];
        top = -1;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }
    
    public void push(int element) {
        stack[++top] = element;
        
    }
    
    public int pop() {
        element = stack[top--];
        return element;
    }
    
    public int getStackSize() {
        return stack.length -1;
    }
     
}
