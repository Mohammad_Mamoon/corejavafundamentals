/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.formsOfConstructors;

import static java.lang.System.out;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ConstructorTest {
    public static void main(String[] args) {
        
        // fill the staff array with Employee objects
        Employee[] staff = new Employee[3];
        
        
        staff[0] = new Employee("Harry",50000);
        staff[1] = new Employee(60000);
        staff[2] = new Employee();
        
        // print out information about Employee objects
        for(Employee e : staff) {
            out.println("Employee name : " + e.getName() + ", id : " + e.getId()  + ", Employee salary : " + e.getSalary() );
        }
    }
}
