/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.formsOfConstructors;

import java.util.Random;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Employee {
    
    
    // these assignments are carried first before the constructor call
    // first to be executed
    private static int nextId;
    
    private int id;
    private String name = "";  // instance field initialization
    private double salary;
    
    
    
    // static field initialization block
    static {
        Random generator = new Random();
        // set nextId to a random number between 0 and 9999
        nextId = generator.nextInt(10000);
        System.out.println("first to be executed");
        
    }
    
    // object initialization block
    {
     id = nextId;
     nextId++;
        System.out.println("second to be executed");
    }
    
    // three overloaded constructors
    public Employee(String n, double s){
        
        name = n;
        salary = s;
    } 
    
    public Employee(double s) {
        // calls the Employee(String, double) constructor
        this("Employee#" + nextId,s);
    }
    
    public Employee(){
        
        // name initialized to "" 
        // salary not explicitly set -- initialized to 0
        // id initialized in initialization block
    }
    
     public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }
    
}
