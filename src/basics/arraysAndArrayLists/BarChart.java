/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.arraysAndArrayLists;

/**
 *
 * @author Mohammad_Mamoon
 */
public class BarChart {
    public static void main(String[] args) {
        
        int[] intArray = { 0,0,0,0,0,0,1,2,4,2,1};
        
        System.out.println("Grade distribution");
        
        // for each element output a bar of the chart
        for(int counter =0; counter<intArray.length; counter++) {
            // output bar label ("00-09","10-19"..."100-109")
            
            if(counter==10)
                System.out.printf("%5d: ",100);
            else
                System.out.printf("%02d-%02d: ",
                        counter*10 ,counter*10 + 9);
            
            // print bar of asterisks
            for(int stars=0; stars < intArray[counter]; stars++)
                System.out.printf("*");
            
            System.out.println();
        }
            
    }
    
}
