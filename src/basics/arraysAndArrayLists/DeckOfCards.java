/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advanced.streams.dietelAndDietel;

import java.security.SecureRandom;

/**
 *
 * @author Mohammad_Mamoon
 */
public class DeckOfCards {
    
    private Card[] deck;  // array of Card objects
    private int currentCard; // index of next card to be dealt with(0-51)
    private static final int NUMBER_OF_CARDS=52;
    
    private static final SecureRandom randomNumbers = new SecureRandom();
    
    public DeckOfCards() {
        String[] faces = { "Ace","Duece","Three","Four","Five","Six",
            "Seven","Eight","Nine","Ten","Jack","Queen","King"};
        String[] suits = { "Hearts","Diamonds","Spades","Clubs"};
        
        deck = new Card[NUMBER_OF_CARDS];   //create an array of card objects
        currentCard=0;  // first Card dealt with will be Deck[0]
        
        // populate deck with card objects
        for(int count=0; count<deck.length; count++){
            deck[count] = new Card(faces[count%13],suits[count/13]);
        
        }
     
    } 
    
    
    public void shuffle() {
        
        currentCard=0;
        
        for(int first=0; first<deck.length; first++) {
            int second= randomNumbers.nextInt(NUMBER_OF_CARDS);
            
            Card temp = deck[first];
            deck[first]= deck[second];
            deck[second] = temp;
        
        }
    }
    
    public Card dealCard() {
        if(currentCard < deck.length)
            return deck[currentCard++];
        else
            return null;
    }
}
    

