/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.arraysAndArrayLists;

import java.util.ArrayList;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayListCollection {
    public static void main(String[] args) {
        
        ArrayList<String> items = new ArrayList<String>(10);
        
        items.add("Green");
        items.add(0, "Orange");
        
        System.out.printf("Display list contents with counter controlled loop:%n");
        
        for(int i=0; i<items.size(); i++)
            System.out.printf(" %s",items.get(i));
        
        // display colors with the enhanced for in the display method
        display(items,"%nDisplay list contents with the enhanced for statement%n");
        
        items.add("Blue");
        items.add("Yellow");
        items.add("Cyan");
        items.add("Voilet");
        display(items,"%nList with two new elements%n");
        
        
        items.remove("Green"); // removes the first occurance of it
        display(items,"%nList without the Green element%n");
        
        items.remove(2);
        display(items,"%nRemoved element at index 2%n");
        
        // check if the element is in the list
        System.out.printf("\"Red\" is %s in the list%n",
                items.contains("Red") ? "" : "not");
        
        // display the size of the list
        System.out.printf("Size: %s%n",items.size());
        
        
        
        
        
        
    }
    
    public static void display(ArrayList<String> items,String str) {
        System.out.printf(str);  // display header
        
        for(String element : items)
            System.out.printf(" %s",element);
        
        System.out.println();
    }
    
}
