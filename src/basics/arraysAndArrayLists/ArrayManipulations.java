/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.arraysAndArrayLists;

import java.util.Arrays;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ArrayManipulations {
    public static void main(String[] args) {
        
        // sort double array into ascending order
        double[] doubleArray = { 2.4, 76.5, 1, 4, 66, 3};
        Arrays.sort(doubleArray);
        System.out.printf("%ndoubleArray: ");
        
        for(double i : doubleArray)
            System.out.printf("%1f ",i);
        
        // fill 10 element array with 7s
        int[] filledIntArray = new int[10];
        Arrays.fill(filledIntArray, 7);
        
        
        int[] intArray = { 1,2,3,4,5,6,7};
        int[] intArrayCopy = new int[intArray.length];
        System.arraycopy(intArray, 0, intArrayCopy, 0, intArray.length);
        displayArray(intArray,"intarray");
        displayArray(intArrayCopy,"intArrayCopy");
        
        // compare intArray and intArrayCopy for equality
        boolean b = Arrays.equals(intArray,intArrayCopy);
        System.out.printf("%n%nintArray %s intArrayCopy%n",
                (b ? "=" : "!="));
        
        // compare intArray and filledIntArray for equality
        b = Arrays.equals(intArray,filledIntArray);
        System.out.printf("%n%nintArray %s filledIntArray%n",
                (b ? "==" : "!="));
        
        // search intArray for value 5
        int location = Arrays.binarySearch(intArray, 2, 5, 5);
        
        if(location >= 0){
            System.out.printf("Found 5 at location %d in intArray%n",
                    location);
        }
        
        else {
            System.out.printf("5 not found in intArray");
        }
                
    }
    
    public static void displayArray(int[] array, String description){
        
        System.out.printf("%n%s: ",description);
        
        for(int i : array)
            System.out.printf("%d ",i);
    }
    
}
