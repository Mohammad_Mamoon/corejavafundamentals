/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.methodReferenceTry;

import java.util.function.BiFunction;

/**
 *
 * @author Mohammad_Mamoon
 */
public class Main {
    
    public static void main(String[] args) {
        
        IsReferable r = new IsReferable() {
          
            @Override
            public void referenceDemo(String a) {
                System.out.println("hello" + a);
            }
        };
        
        r.referenceDemo("5");
        
        
        IsReferable r1 = (x) ->
                
                System.out.println("Saturday"+ x);
        
        r1.referenceDemo("5");
        
        
        IsReferable r2 = (x) -> {
          
            ReferenceDemo.commonMethod(x);
        };
        
        r2.referenceDemo("5");
        
        
        IsReferable r3 = ReferenceDemo::commonMethod;
        r3.referenceDemo("4");
        r3.greet();
        IsReferable.call();
        
        
        BiFunction<Integer,Integer,Integer> add = new BiFunction<Integer,Integer,Integer>() {
          
            @Override
            public Integer apply(Integer a ,Integer b) {
                return a+b; 
                        
            }
            
        };
        
        System.out.println("sum = " + add.apply(2, 30));
        
        
        
        
        BiFunction<Integer,Integer,Integer> add1 = (a,b) ->  a+b;
        System.out.println("sum = " + add1.apply(2, 3));
        
        ArithmeticClass obj = new ArithmeticClass();
        
        BiFunction<Integer,Integer,Integer> add2 = (a,b) -> {
            
             return obj.sum(a,b);
        };
        
        System.out.println("sum =" + add2.apply(33,1));
        
        BiFunction<Integer,Integer,Integer> sub = obj::sub;
        System.out.println("sub =" + sub.apply(4, 3) );
        
        
    }
    
    
    
    
}
