/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.workingWithConstructors;

/**
 *
 * @author Mohammad_Mamoon
 */
public class EmployeeTest {
    
    public static void main(String[] args) {
        
        Employee obj = new Employee();
        //Employee obj = new Employee("Mamoon","Hawal");
        System.out.println(obj.getName());
        System.out.println(obj.getAddress());
    }
}
