/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basics.workingWithConstructors;

/**
 *
 * @author Mohammad_Mamoon
 */
    class Employee {
    
    private String name;
    private String address="Naseem Bagh";
    private int pinCode;

    public Employee() {
        this("mama", "japan");
        System.out.println("inside the default constructor");
        System.out.println(name);
        System.out.println(address);
        System.out.println(pinCode);
    }
    
    public Employee(String n,String address){
//        this();
        System.out.println("inside parameterized constructor");
        System.out.println(name = n);
        System.out.println(this.address = address);
        System.out.println(pinCode);
    }
    
    
     public String getName() {
        return name;
    }
     

    public String getAddress() {
        return address;
    }
    
    
}
