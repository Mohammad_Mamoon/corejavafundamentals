/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversionArrayListToLinkedList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mohammad_Mamoon
 */
public class BFM {
    public static void main(String[] args) {
        
        List<String> al = new ArrayList<String>();
        al.add("Geek");
        al.add("for");
        al.add("geeks");
        
        System.out.println(al);
        
        List<String> ll = convertToLinkedList(al);
        
        System.out.println(ll);
    }
    
    
    private static <T> List<T> convertToLinkedList(List<T> list) {
       
        List<T> ll = new LinkedList<T>();
        
        for( T element : list) {
            ll.add(element);
        }
        
        return ll;
    }
    
       
    
}
