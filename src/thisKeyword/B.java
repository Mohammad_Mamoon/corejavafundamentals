/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thisKeyword;

/**
 *
 * @author Mohammad_Mamoon
 */
public class B {
    
    int num = 10;
    
    B() {
        
        // this is used in the constructor call . its useful in using one object
        // in multiple classes
        A obj = new A(this);
        obj.display();
    }
    
    public static void main(String[] args) {
        B obj = new B();
    }
    
}
