/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thisKeyword;

/**
 *
 * @author Mohammad_Mamoon
 */
public class ThisKey {
    
    private int id;
    private String name;
    
    
    ThisKey(int id , String name) {
        
        // referencing constructor
        this();
        // referencing class instance variables
        this.id = id;
        this.name = name;
    }
    
    ThisKey() {
        System.out.println("calling default constructor");
    }
    
    
    public void display() {
        System.out.println("your name is : " + name + " with id " + id);
        // referencing class methods
        this.message();
    }
    
    public void message() {
        System.out.println("hello there");
        
        // passing this as an argument
        pass(this);
    }
    
    public void pass(ThisKey obj2) {
        System.out.println("name: " + obj2.name);
    }
    
}
